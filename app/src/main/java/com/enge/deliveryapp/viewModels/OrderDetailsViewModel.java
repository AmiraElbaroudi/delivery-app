package com.enge.deliveryapp.viewModels;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.enge.deliveryapp.Controls.AppSettings;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.Remote.ApiClient;
import com.enge.deliveryapp.Remote.ApiInterface;
import com.enge.deliveryapp.model.OrderDetails;
import com.google.gson.Gson;

import java.util.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class OrderDetailsViewModel  extends Observable {
    Gson gson = new Gson();
    private Context context;

    private OrderDetails details = new OrderDetails();

    private MutableLiveData<String> toastMessageOrderDetails= new MutableLiveData<>();
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public OrderDetailsViewModel(Context context) {
        this.context = context;
    }


    public void getOrderDetails(int orderId) {

        isLoading.setValue(true);

        ApiClient.getClient().create(ApiInterface.class)
                .getOrderDetails(orderId, AppSettings.getAuthorization(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<OrderDetails>>() {
                    @Override
                    public void onNext(@NonNull Response<OrderDetails> object) {

                        if (object.raw().code() == 200) {
                            if (object.body() != null) {

                                details = object.body();

                            } else {

                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        isLoading.setValue(false);
                        toastMessageOrderDetails.setValue(context.getString(R.string.failed));

                        setChanged();//notify view
                        notifyObservers();

                    }

                    @Override
                    public void onComplete() {
                        isLoading.setValue(false);

                        setChanged();//notify view
                        notifyObservers();
                        //toastMessageOrderDetails.setValue(context.getString(R.string.complete));

                    }
                });
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }
    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public MutableLiveData<String> getToastMessageOrderDetails() {
        if (toastMessageOrderDetails == null) {
            toastMessageOrderDetails = new MutableLiveData<>() ;
        }
        return toastMessageOrderDetails;
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }
    public void setIsLoading(MutableLiveData<Boolean> isLoading) {
        this.isLoading = isLoading;
    }

    public OrderDetails getDetails() {
        return details;
    }
    public void setDetails(OrderDetails details) {
        this.details = details;
    }
}
