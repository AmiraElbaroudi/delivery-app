package com.enge.deliveryapp.viewModels;

import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;


import com.enge.deliveryapp.Controls.AppSettings;
import com.enge.deliveryapp.Helpers.ActivityHelper;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.Remote.ApiClient;
import com.enge.deliveryapp.Remote.ApiInterface;
import com.enge.deliveryapp.model.DeliveryBoys;
import com.enge.deliveryapp.model.Order;

import com.enge.deliveryapp.model.OrderDistributor;
import com.enge.deliveryapp.model.UserData;
import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class OrdersViewModel extends Observable {

    Gson gson = new Gson();
    private Context context;

    private ArrayList<Order> deliverdOrdersArr = new ArrayList<>();
    private ArrayList<Order> preparedOrdersArr = new ArrayList<>();
    private ArrayList<Order> lateOrdersArr = new ArrayList<>();
    private String time;

    private ArrayList<Order> deliverdOrdersDeliveryboy ;
    private ArrayList<Order> notDeliverdOrdersArr ;
    private ArrayList<Order> lateOrdersDeliveryboy ;
    private ArrayList<Order> notAcceptedOrderArr ;

    private DeliveryBoys deliveryBoys ;

    private boolean onAllOrdersClicked = false;

    private MutableLiveData<String> toastMessage = new MutableLiveData<>();
    private MutableLiveData<Boolean> isLoadingOrders = new MutableLiveData<>();
    private MutableLiveData<Boolean> isLoadingDeliveryBoy = new MutableLiveData<>();

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public OrdersViewModel(@NonNull Context context) {
        this.context = context;
    }

    public void getAllOrdersDistributor() {

        if (!onAllOrdersClicked){

            onAllOrdersClicked =true;
            isLoadingOrders.setValue(true);

            ApiClient.getClient().create(ApiInterface.class)
                    .getAllOrdersDistributor(AppSettings.getAuthorization(context))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                    new DisposableObserver<Response<OrderDistributor>>() {
                        @Override
                         public void onNext(@NonNull Response<OrderDistributor> object) {

                            if (object.raw().code() == 200) {
                                if (object.body() != null) {

                                    OrderDistributor orderDistributor =  object.body();
                                    deliverdOrdersArr = orderDistributor.getDeliverdOrders();
                                     preparedOrdersArr = orderDistributor.getPreparedOrders();
                                     lateOrdersArr     = orderDistributor.getLateOrders();
                                     time              = orderDistributor.getTime();



//                                    for(int k=0;k<lateOrdersArr.size();k++){
//                                        orders.add(lateOrdersArr.get(k));
//                                    }
//
//                                    for(int j=0;j<preparedOrdersArr.size();j++){
//                                        orders.add(preparedOrdersArr.get(j));
//                                    }
//
//                                    for(int i=0;i<deliverdOrdersArr.size();i++){
//                                        orders.add(deliverdOrdersArr.get(i));
//                                    }

                                } else {

                                }
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            onAllOrdersClicked =false;
                            isLoadingOrders.setValue(false);
                            toastMessage.setValue(context.getString(R.string.failed));

                            setChanged();//notify view
                            notifyObservers();

                            FirebaseCrash.report(e);

                        }

                        @Override
                        public void onComplete() {
                            onAllOrdersClicked =false;
                            isLoadingOrders.setValue(false);

                            setChanged();//notify view
                            notifyObservers();
                        }
                    });


        }
    }

    public void getAllOrdersCarMotor() {


        if (!onAllOrdersClicked){

            onAllOrdersClicked =true;
            isLoadingOrders.setValue(true);

            ApiClient.getClient().create(ApiInterface.class)
                    .getAllOrdersCarMotor(AppSettings.getAuthorization(context))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                    new DisposableObserver<Response<LinkedTreeMap<String,ArrayList<Order>>>> () {
                        @Override
                        public void onNext(@NonNull Response<LinkedTreeMap<String,ArrayList<Order>>> object) {

                            if (object.raw().code() == 200) {
                                if (object.body() != null) {

                                    //TODO Done #5 add the rest of orders latedOrders allnotAcceptedOrder
                                     deliverdOrdersDeliveryboy = object.body().get("allDeliveredOrder");
                                     notDeliverdOrdersArr = object.body().get("allNotDeliveredOrder");
                                     lateOrdersDeliveryboy = object.body().get("latedOrders");
                                     notAcceptedOrderArr = object.body().get("allnotAcceptedOrder");

                                } else {

                                }
                            }
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            onAllOrdersClicked =false;
                            isLoadingOrders.setValue(false);
                            toastMessage.setValue(context.getString(R.string.failed));

                            setChanged();//notify view
                            notifyObservers();

                            FirebaseCrash.report(e);

                        }

                        @Override
                        public void onComplete() {
                            onAllOrdersClicked =false;
                            isLoadingOrders.setValue(false);

                            setChanged();//notify view
                            notifyObservers();
                        }
                    });
        }
    }

    private JsonObject getOrderTime(String time) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObj_ = new JSONObject();
            jsonObj_.put("preparationResTime", time);
            JsonParser jsonParser = new JsonParser();
            gsonObject = (JsonObject) jsonParser.parse(jsonObj_.toString());
            return gsonObject;

        } catch (JSONException e) {
            e.printStackTrace();
            FirebaseCrash.report(e);

            return null;
        }
    }

    public void changeOrderTime(String time) {

        ApiClient.getClient().create(ApiInterface.class)
                .changeOrdersTime(getOrderTime(time), AppSettings.getAuthorization(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<Object>>() {
                    @Override
                    public void onNext(@NonNull Response<Object> object) {

                        if (object.raw().code() == 200) {
                            if (object.body() != null) {

                                LinkedTreeMap response = ((LinkedTreeMap) object.body());

                                if(response.containsKey("saved")){
                                    if((boolean)response.get("saved")){
                                       // getAllOrdersDistributor();

                                    }else {
                                        //  getToastMessage().setValue(context.getString(R.string.error));
                                    }
                                }
                            } else {
                                // getToastMessage().setValue(context.getString(R.string.failed));
                            }

                        }

                        if (object.raw().code() == 401) {
                            ActivityHelper.goToLoginActivity(context);

                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                        setChanged();//notify view
                        notifyObservers();
                        FirebaseCrash.report(e);


                    }

                    @Override
                    public void onComplete() {
//                        setChanged();//notify view
//                        notifyObservers();

                    }
                });
    }

    public  void  getAllCarMotorStatus(){

        isLoadingDeliveryBoy.setValue(true);

        ApiClient.getClient().create(ApiInterface.class)
                .getCarMotorStatus(AppSettings.getAuthorization(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<DeliveryBoys>>() {
                    @Override
                    public void onNext(@NonNull Response<DeliveryBoys> object) {

                        if (object.raw().code() == 200) {
                            if (object.body() != null) {
                                deliveryBoys = object.body();

                            }
                            } else {

                            }
                        }


                    @Override
                    public void onError(@NonNull Throwable e) {
                        isLoadingDeliveryBoy.setValue(false);
                        toastMessage.setValue(context.getString(R.string.failed));

                        setChanged();//notify view
                        notifyObservers();
                        FirebaseCrash.report(e);

                    }

                    @Override
                    public void onComplete() {
                        isLoadingDeliveryBoy.setValue(false);

                        setChanged();//notify view
                        notifyObservers();
                    }
                });


    }

//-------------------------------------------------------------------------------------------

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }
    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public MutableLiveData<String> getToastMessage() {
        if (toastMessage == null) {
            toastMessage = new MutableLiveData<String>();
        }
        return toastMessage;
    }

    public MutableLiveData<Boolean> getIsLoadingOrders() {
        return isLoadingOrders;
    }
    public void setIsLoadingOrders(MutableLiveData<Boolean> isLoadingOrders) {
        this.isLoadingOrders = isLoadingOrders;
    }

    public MutableLiveData<Boolean> getIsLoadingDeliveryBoy() {
        return isLoadingDeliveryBoy;
    }
    public void setIsLoadingDeliveryBoy(MutableLiveData<Boolean> isLoadingDeliveryBoy) {
        this.isLoadingDeliveryBoy = isLoadingDeliveryBoy;
    }

//------------------------ Distributor Orders --------------------------------------

    public ArrayList<Order> getDeliverdOrdersArr() {
        return deliverdOrdersArr;
    }
    public void setDeliverdOrdersArr(ArrayList<Order> deliverdOrdersArr) {
        this.deliverdOrdersArr = deliverdOrdersArr;
    }

    public ArrayList<Order> getPreparedOrdersArr() {
        return preparedOrdersArr;
    }
    public void setPreparedOrdersArr(ArrayList<Order> preparedOrdersArr) {
        this.preparedOrdersArr = preparedOrdersArr;
    }

    public ArrayList<Order> getLateOrdersArr() {
        return lateOrdersArr;
    }
    public void setLateOrdersArr(ArrayList<Order> lateOrdersArr) {
        this.lateOrdersArr = lateOrdersArr;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

//------------------------ Delivery boy Orders --------------------------------------

    public ArrayList<Order> getDeliverdOrdersDeliveryboy() {
        return deliverdOrdersDeliveryboy;
    }
    public void setDeliverdOrdersDeliveryboy(ArrayList<Order> deliverdOrdersDeliveryboy) {
        this.deliverdOrdersDeliveryboy = deliverdOrdersDeliveryboy;
    }

    public ArrayList<Order> getNotDeliverdOrdersArr() {
        return notDeliverdOrdersArr;
    }
    public void setNotDeliverdOrdersArr(ArrayList<Order> notDeliverdOrdersArr) {
        this.notDeliverdOrdersArr = notDeliverdOrdersArr;
    }

    public ArrayList<Order> getLateOrdersDeliveryboy() {
        return lateOrdersDeliveryboy;
    }
    public void setLateOrdersDeliveryboy(ArrayList<Order> lateOrdersDeliveryboy) {
        this.lateOrdersDeliveryboy = lateOrdersDeliveryboy;
    }

    public ArrayList<Order> getNotAcceptedOrderArr() {
        return notAcceptedOrderArr;
    }
    public void setNtAcceptedOrderArr(ArrayList<Order> notAcceptedOrderArr) {
        this.notAcceptedOrderArr = notAcceptedOrderArr;
    }


    public DeliveryBoys getDeliveryBoys() {
        return deliveryBoys;
    }
    public void setDeliveryBoys(DeliveryBoys deliveryBoys) {
        this.deliveryBoys = deliveryBoys;
    }


}
