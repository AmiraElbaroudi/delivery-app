package com.enge.deliveryapp.viewModels;

import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.enge.deliveryapp.Activities.HomeActivity;
import com.enge.deliveryapp.Controls.AppSettings;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.Remote.ApiClient;
import com.enge.deliveryapp.Remote.ApiInterface;
import com.enge.deliveryapp.model.OrderData;
import com.enge.deliveryapp.model.RestaurantData;
import com.enge.deliveryapp.model.UserCoord;
import com.enge.deliveryapp.model.UserData;
import com.google.firebase.crash.FirebaseCrash;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class AddressViewModel extends Observable {

    Gson gson = new Gson();
    private Context context;

    private RestaurantData restaurantData = new RestaurantData();
    private UserData userData = new UserData();
    private UserCoord userCoord = new UserCoord();

    private MutableLiveData<String> toastMessage;

    private MutableLiveData<Boolean> isLoadingOrderDataDistributor = new MutableLiveData<>();
    private MutableLiveData<Boolean> isLoadingOrderData = new MutableLiveData<>();

    private MutableLiveData<Boolean> isAccept = new MutableLiveData<>();
    private MutableLiveData<Boolean> isCheckIn = new MutableLiveData<>();
    private MutableLiveData<Boolean> isTake = new MutableLiveData<>();
    private MutableLiveData<Boolean> isDeliverToCustomer = new MutableLiveData<>();

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public AddressViewModel(@NonNull Context context) {
        this.context = context;
    }

    public void getOrderDataDataDeliveryBoy(int orderId) {
        isLoadingOrderData.setValue(true);

        ApiClient.getClient().create(ApiInterface.class)
                .getOrderDataDeliveryBoy(orderId, AppSettings.getAuthorization(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<OrderData>>() {
                    @Override
                    public void onNext(@NonNull Response<OrderData> object) {

                        if (object.raw().code() == 200) {

                            if (object.body() != null) {

                                restaurantData = object.body().getRestaurant();
                                userData = object.body().getUser();
                                userCoord = object.body().getUserCoord();

                            } else {

                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        isLoadingOrderData.setValue(false);
                        toastMessage.setValue(context.getString(R.string.failed));

                        setChanged();//notify view
                        notifyObservers();
                        FirebaseCrash.report(e);

                    }

                    @Override
                    public void onComplete() {
                        isLoadingOrderData.setValue(false);

                        setChanged();//notify view
                        notifyObservers();
                        //toastMessage.setValue(context.getString(R.string.complete));

                    }
                });
    }

    public void getOrderDataDistributor(int orderId){

        isLoadingOrderDataDistributor.setValue(true);

        ApiClient.getClient().create(ApiInterface.class)
                .getOrderDataDistributor(orderId, AppSettings.getAuthorization(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<OrderData>>() {
                    @Override
                    public void onNext(@NonNull Response<OrderData> object) {

                        if (object.raw().code() == 200) {

                            if (object.body() != null) {

                                restaurantData = object.body().getRestaurant();
                                userData = object.body().getUser();
                                userCoord = object.body().getUserCoord();

                            } else {

                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        isLoadingOrderDataDistributor.setValue(false);
                        toastMessage.setValue(context.getString(R.string.failed));

                        setChanged();//notify view
                        notifyObservers();
                        FirebaseCrash.report(e);

                    }

                    @Override
                    public void onComplete() {
                        isLoadingOrderDataDistributor.setValue(false);

                        setChanged();//notify view
                        notifyObservers();
                        //toastMessage.setValue(context.getString(R.string.complete));

                    }
                });

    }

    public void sendOrderForAllCars(int orderId) {

        ApiClient.getClient().create(ApiInterface.class)
                .sendOrderForAllCars(orderId, AppSettings.getAuthorization(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<Object>>() {
                    @Override
                    public void onNext(@NonNull Response<Object> object) {

                        if (object.raw().code() == 200) {
                            if (object.body() != null) {

                                LinkedTreeMap response = ((LinkedTreeMap) object.body());

                                if (response.containsKey("saved")) {
                                    if ((boolean) response.get("saved")) {
                                        getToastMessage().setValue(context.getString(R.string.assign_order));

                                    } else {
                                        toastMessage.setValue(context.getString(R.string.failed));

                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        toastMessage.setValue(context.getString(R.string.failed));

                        setChanged();//notify view
                        notifyObservers();
                        FirebaseCrash.report(e);

                    }

                    @Override
                    public void onComplete() {
                        setChanged();//notify view
                        notifyObservers();

                    }
                });
    }

    public void sendOrderForAllMotors(int orderId) {

        ApiClient.getClient().create(ApiInterface.class)
                .sendOrderForAllMotors(orderId, AppSettings.getAuthorization(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<Object>>() {
                    @Override
                    public void onNext(@NonNull Response<Object> object) {

                        if (object.raw().code() == 200) {
                            if (object.body() != null) {

                                LinkedTreeMap response = ((LinkedTreeMap) object.body());

                                if (response.containsKey("saved")) {
                                    if ((boolean) response.get("saved")) {
                                        getToastMessage().setValue(context.getString(R.string.assign_order));

                                    } else {
                                        toastMessage.setValue(context.getString(R.string.failed));

                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        toastMessage.setValue(context.getString(R.string.failed));

                        setChanged();//notify view
                        notifyObservers();
                        FirebaseCrash.report(e);

                    }

                    @Override
                    public void onComplete() {
                        setChanged();//notify view
                        notifyObservers();

                    }
                });
    }

    public void assignOrderToDeliveryBoy(int orderId, int deliverBoyId) {

        ApiClient.getClient().create(ApiInterface.class)
                .assignOrderToDeliveryBoy(orderId, deliverBoyId, AppSettings.getAuthorization(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<Object>>() {
                    @Override
                    public void onNext(@NonNull Response<Object> object) {

                        if (object.raw().code() == 200) {
                            if (object.body() != null) {

                                LinkedTreeMap response = ((LinkedTreeMap) object.body());

                                if (response.containsKey("saved")) {
                                    if ((boolean) response.get("saved")) {
                                        getToastMessage().setValue(context.getString(R.string.assign_order));

                                    } else {
                                        toastMessage.setValue(context.getString(R.string.failed));

                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        toastMessage.setValue(context.getString(R.string.failed));

                        setChanged();//notify view
                        notifyObservers();
                        FirebaseCrash.report(e);

                    }

                    @Override
                    public void onComplete() {
                        setChanged();//notify view
                        notifyObservers();

                    }
                });

    }


    public void acceptOrder(int orderId) {
        isAccept.setValue(true);

        ApiClient.getClient().create(ApiInterface.class)
                .acceptOrder(orderId, AppSettings.getAuthorization(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<Object>>() {
                    @Override
                    public void onNext(@NonNull Response<Object> object) {
                        LinkedTreeMap response = ((LinkedTreeMap) object.body());

                        if (object.raw().code() == 200) {
                            if (object.body() != null) {

                                if ((boolean) response.get("saved")) {
                                    toastMessage.setValue(context.getString(R.string.accept_order));
                                }


                            } else {

                            }
                        } else {
                            if(object.errorBody() != null) {

                                String result = null;
                                try {
                                    result = object.errorBody().string().toString();
                                    String[] subResult = result.split("modelState");
                                    String s =subResult[1];
                                    s = s.substring(s.indexOf("[")+2);
                                    s = s.substring(0, s.indexOf("\""));
                                    toastMessage.setValue(s);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    FirebaseCrash.report(e);
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        isAccept.setValue(false);
                        toastMessage.setValue(context.getString(R.string.failed));

                        setChanged();//notify view
                        notifyObservers();
                        FirebaseCrash.report(e);

                    }

                    @Override
                    public void onComplete() {
                        isAccept.setValue(false);

                        setChanged();//notify view
                        notifyObservers();
                        //toastMessage.setValue(context.getString(R.string.complete));

                    }
                });

    }

    private JsonObject getLocation(int orderId, String lon, String lat) {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObj_ = new JSONObject();

            jsonObj_.put("OrderId", orderId);
            jsonObj_.put("Long", lon);
            jsonObj_.put("Lat", lat);

            JsonParser jsonParser = new JsonParser();
            gsonObject = (JsonObject) jsonParser.parse(jsonObj_.toString());
            return gsonObject;

        } catch (JSONException e) {
            e.printStackTrace();
            FirebaseCrash.report(e);

            return null;
        }
    }

    public void checkInOrders(int orderId, String lon, String lat) {
        isCheckIn.setValue(true);

        ApiClient.getClient().create(ApiInterface.class)
                .checkInOrders(getLocation(orderId, lon, lat), AppSettings.getAuthorization(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<Object>>() {
                    @Override
                    public void onNext(@NonNull Response<Object> object) {
                        LinkedTreeMap response = ((LinkedTreeMap) object.body());

                        if (object.raw().code() == 200) {
                            if (object.body() != null) {
                                if ((boolean) response.get("saved")) {
                                    toastMessage.setValue(context.getString(R.string.check_in));
                                }
                            } else {

                            }
                        }else {
                            if(object.errorBody() != null) {

                                String result = null;
                                try {
                                    result = object.errorBody().string().toString();
                                    String[] subResult = result.split("modelState");
                                    String s =subResult[1];
                                    s = s.substring(s.indexOf("[")+2);
                                    s = s.substring(0, s.indexOf("\""));
                                    toastMessage.setValue(s);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    FirebaseCrash.report(e);
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        isCheckIn.setValue(false);
                        toastMessage.setValue(context.getString(R.string.failed));

                        setChanged();//notify view
                        notifyObservers();
                        FirebaseCrash.report(e);

                    }

                    @Override
                    public void onComplete() {
                        isCheckIn.setValue(false);

                        setChanged();//notify view
                        notifyObservers();
//                    toastMessage.setValue(context.getString(R.string.complete));

                    }
                });

    }

    public void takeOrder(int orderId) {
        isTake.setValue(true);

        ApiClient.getClient().create(ApiInterface.class)
                .takeOrder(orderId, AppSettings.getAuthorization(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<Object>>() {
                    @Override
                    public void onNext(@NonNull Response<Object> object) {
                        LinkedTreeMap response = ((LinkedTreeMap) object.body());

                        if (object.raw().code() == 200) {
                            if (object.body() != null) {
                                if ((boolean) response.get("saved")) {
                                    toastMessage.setValue(context.getString(R.string.take));

//                                Intent returnIntent = new Intent(context, AddressActivity.class);
//                                returnIntent.putExtra("result",context.getResources().getString(R.string.deliver_to_customer));
//                                ((Activity) context).setResult(RESULT_OK,returnIntent);
//                                ((Activity) context).finish();

                                }


                            } else {

                            }
                        } else {
                            if(object.errorBody() != null) {

                                String result = null;
                                try {
                                    result = object.errorBody().string().toString();
                                    String[] subResult = result.split("modelState");
                                    String s =subResult[1];
                                    s = s.substring(s.indexOf("[")+2);
                                    s = s.substring(0, s.indexOf("\""));
                                    toastMessage.setValue(s);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    FirebaseCrash.report(e);
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        isTake.setValue(false);
                        toastMessage.setValue(context.getString(R.string.failed));

                        setChanged();//notify view
                        notifyObservers();
                        FirebaseCrash.report(e);

                        Intent intent = new Intent(context, HomeActivity.class);
                        ((Activity) context).startActivity(intent);
                        ((Activity) context).finish();
                    }

                    @Override
                    public void onComplete() {
                        isTake.setValue(false);

                        setChanged();//notify view
                        notifyObservers();
//                    toastMessage.setValue(context.getString(R.string.complete));
                    }
                });

    }

    public void deliverToCustomer(int orderId, String lon, String lat) {
        isDeliverToCustomer.setValue(true);

        ApiClient.getClient().create(ApiInterface.class)
                .deliverToCustomer(getLocation(orderId, lon, lat), AppSettings.getAuthorization(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<Object>>() {
                    @Override
                    public void onNext(@NonNull Response<Object> object) {

                        LinkedTreeMap response = ((LinkedTreeMap) object.body());

                        if (object.raw().code() == 200) {
                            if (object.body() != null) {
                                if ((boolean) response.get("saved")) {
                                    toastMessage.setValue(context.getString(R.string.deliver_to_customer));
                                }


                            } else {

                            }
                        } else {
                            if(object.errorBody() != null) {

                                String result = null;
                                try {
                                    result = object.errorBody().string().toString();
                                    String[] subResult = result.split("modelState");
                                    String s =subResult[1];
                                    s = s.substring(s.indexOf("[")+2);
                                    s = s.substring(0, s.indexOf("\""));
                                    toastMessage.setValue(s);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                    FirebaseCrash.report(e);
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        isDeliverToCustomer.setValue(false);
                        toastMessage.setValue(context.getString(R.string.failed));

                        setChanged();//notify view
                        notifyObservers();
                        FirebaseCrash.report(e);

                    }

                    @Override
                    public void onComplete() {
                        isDeliverToCustomer.setValue(false);

                        setChanged();//notify view
                        notifyObservers();
                        //toastMessage.setValue(context.getString(R.string.complete));

                    }
                });

    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public MutableLiveData<String> getToastMessage() {
        if (toastMessage == null) {
            toastMessage = new MutableLiveData<String>();
        }
        return toastMessage;
    }

    public MutableLiveData<Boolean> getIsLoadingOrderDataDistributor() {
        return isLoadingOrderDataDistributor;
    }

    public void setIsLoadingOrderDataDistributor(MutableLiveData<Boolean> isLoadingOrderDataDistributor) {
        this.isLoadingOrderDataDistributor = isLoadingOrderDataDistributor;
    }

    public MutableLiveData<Boolean> getIsLoadingOrderData() {
        return isLoadingOrderData;
    }

    public void setIsLoadingOrderData(MutableLiveData<Boolean> isLoadingOrderData) {
        this.isLoadingOrderData = isLoadingOrderData;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public RestaurantData getRestaurantData() {
        return restaurantData;
    }

    public void setRestaurantData(RestaurantData restaurantData) {
        this.restaurantData = restaurantData;
    }

    public UserCoord getUserCoord() {
        return userCoord;
    }

    public void setUserCoord(UserCoord userCoord) {
        this.userCoord = userCoord;
    }

    public MutableLiveData<Boolean> getIsAccept() {
        return isAccept;
    }

    public void setIsAccept(MutableLiveData<Boolean> isAccept) {
        this.isAccept = isAccept;
    }

    public MutableLiveData<Boolean> getIsCheckIn() {
        return isCheckIn;
    }

    public void setIsCheckIn(MutableLiveData<Boolean> isCheckIn) {
        this.isCheckIn = isCheckIn;
    }

    public MutableLiveData<Boolean> getIsTake() {
        return isTake;
    }

    public void setIsTake(MutableLiveData<Boolean> isTake) {
        this.isTake = isTake;
    }

    public MutableLiveData<Boolean> getIsDeliverToCustomer() {
        return isDeliverToCustomer;
    }

    public void setIsDeliverToCustomer(MutableLiveData<Boolean> isDeliverToCustomer) {
        this.isDeliverToCustomer = isDeliverToCustomer;
    }
}
