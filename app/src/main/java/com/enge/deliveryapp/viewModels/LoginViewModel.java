package com.enge.deliveryapp.viewModels;

import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.Editable;

import com.enge.deliveryapp.Activities.HomeActivity;
import com.enge.deliveryapp.Controls.AppSettings;
import com.enge.deliveryapp.Helpers.ActivityHelper;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.Remote.ApiClient;
import com.enge.deliveryapp.Remote.ApiInterface;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;


public class LoginViewModel extends Observable {

    FirebaseAuth auth;
    DatabaseReference reference;

    Gson gson = new Gson();
    private String username, password;
    private boolean loginClicked = false;
    private boolean logoutClicked = false;
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MutableLiveData<String> toastMessage;
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>();

    public LoginViewModel(@NonNull Context context) {
        this.context = context;
    }

    public void onLoginClicked() {

       saveRegistrationId();

        if (!loginClicked) {

            loginClicked = true;

            String grantType = "password";

            if (username == null || password == null) {
                getToastMessage().setValue(context.getString(R.string.email_password_required));
                return;
            }

            if (username != null && username.length() == 0) {
                getToastMessage().setValue(context.getString(R.string.email_password_required));
                return;
            }
            if (password != null && password.length() == 0) {
                getToastMessage().setValue(context.getString(R.string.email_password_required));
                return;
            }
            saveRegistrationId();
            login(username, password, grantType);
        }
    }
    public void OnEmailChange(@NonNull Editable editable) {
        try {
            username = editable.toString();
        } catch (Exception e) {
            username = null;
            e.printStackTrace();
            FirebaseCrash.report(e);
        }
    }
    public void OnPasswordChange(@NonNull Editable editable) {
        try {
            password = editable.toString();
        } catch (Exception e) {
            password = null;
            e.printStackTrace();
            FirebaseCrash.report(e);

        }
    }

    private void login(final String username, final String password, String grantType) {

        isLoading.setValue(true);

        ApiClient.getClient().create(ApiInterface.class)
                .login(username, password, grantType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<Object>>() {
                    @Override
                    public void onNext(@NonNull Response<Object> object) {


                        if (object.raw().code() == 200) {
                            if (object.body() != null) {

                                String access_token = ((String) ((LinkedTreeMap) object.body()).get("access_token"));
                                String role =(String) ((LinkedTreeMap) object.body()).get("Role");
                                String id =(String) ((LinkedTreeMap) object.body()).get("Id");

                                AppSettings.setLoggedIn(context,true);
                                AppSettings.setAccessToken(context, access_token);
                                AppSettings.setRole(context, role);
                                AppSettings.setId(context, id);
                                AppSettings.setUserName(context,username);
                                AppSettings.setPassword(context,password);


                                toastMessage.setValue(context.getString(R.string.login_successfully));

                                ((Activity) context).startActivity(new Intent(context, HomeActivity.class));

                                ((Activity) context).finish();

                            } else {
                                //serverError
                                toastMessage.setValue(context.getString(R.string.server_error));
                            }

                            //TODO done # 3 handle id user entered incorrect username and passord. show a toast message or make error flag on edittext using setError().

                        }else if (object.raw().code() == 400){//incorrect username and password

                            if (object.errorBody() != null) {

                                toastMessage.setValue(context.getString(R.string.invalid_grant));

                            }else {

                            }


                        }else if (object.raw().code() == 401) {
                            ActivityHelper.goToLoginActivity(context);

                        } else  if (object.raw().code() == 500) {
                            //serverError
                            toastMessage.setValue(context.getString(R.string.server_error));
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        loginClicked = false;
                        isLoading.setValue(false);
                        toastMessage.setValue(context.getString(R.string.failed));
                        setChanged();//notify view
                        notifyObservers();

                        FirebaseCrash.report(e);
                    }

                    @Override
                    public void onComplete() {
                        isLoading.setValue(false);
                        loginClicked = false;

                       // toastMessage.setValue(context.getString(R.string.server_error));

                        setChanged();//notify view
                        notifyObservers();

                    }
                });
    }

    public void onLogoutClicked() {
        if (!logoutClicked) {
            logoutClicked = true;
            logOut();
        }
    }
    private JsonObject getRegistrationId() {
        JsonObject gsonObject = new JsonObject();
        try {
            JSONObject jsonObj_ = new JSONObject();
            jsonObj_.put("RegistrationId", FirebaseInstanceId.getInstance().getToken());

            JsonParser jsonParser = new JsonParser();
            gsonObject = (JsonObject) jsonParser.parse(jsonObj_.toString());
            return gsonObject;

        } catch (JSONException e) {
            e.printStackTrace();
            FirebaseCrash.report(e);
            return null;
        }
    }

    private void saveRegistrationId() {

        ApiClient.getClient().create(ApiInterface.class)
                .saveRegistrationId(getRegistrationId(), AppSettings.getAuthorization(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<Object>>() {
                    @Override
                    public void onNext(@NonNull Response<Object> object) {

                        if (object.raw().code() == 200) {

                        }
                        if (object.raw().code() == 401) {

                        }

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        //setChanged();//notify view
                        //notifyObservers();
                        FirebaseCrash.report( e);

                    }

                    @Override
                    public void onComplete() {
                        //setChanged();//notify view
                        //notifyObservers();

                    }
                });


    }
    private void logOut() {

        ApiClient.getClient().create(ApiInterface.class)
                .logout(getRegistrationId(), AppSettings.getAuthorization(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                new DisposableObserver<Response<Object>>() {
                    @Override
                    public void onNext(@NonNull Response<Object> object) {

                        logoutClicked = false;
                        if (object.raw().code() == 200) {
                            if (object.body() != null) {

                                LinkedTreeMap response = ((LinkedTreeMap) object.body());

                                if(response.containsKey("saved")){
                                    if((boolean)response.get("saved")){

                                        getToastMessage().setValue(context.getString(R.string.logout_successfully));

                                        AppSettings.setLoggedIn(context, false);
                                        ((Activity) context).finish();

                                        ActivityHelper.goToLoginActivity(context);


                                    }else {
                                        getToastMessage().setValue(context.getString(R.string.error));                                    }
                                }
                            } else {
                                toastMessage.setValue(context.getString(R.string.server_error));
                            }
                        }

                        if (object.raw().code() == 401) {
                            ActivityHelper.goToLoginActivity(context);
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        logoutClicked = false;
                        toastMessage.setValue(context.getString(R.string.failed));
                        setChanged();//notify view
                        notifyObservers();
                        FirebaseCrash.report(e);

                    }

                    @Override
                    public void onComplete() {
                        loginClicked = false;

                        setChanged();//notify view
                        notifyObservers();
                    }
                });


    }


    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }
    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public MutableLiveData<String> getToastMessage() {
        if (toastMessage == null) {
            toastMessage = new MutableLiveData<String>();
        }
        return toastMessage;
    }
    public MutableLiveData<Boolean> getIsLoading() {
        return isLoading;
    }
    public void setIsLoading(MutableLiveData<Boolean> isLoading) {
        this.isLoading = isLoading;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

}
