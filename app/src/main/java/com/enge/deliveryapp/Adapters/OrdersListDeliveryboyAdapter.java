package com.enge.deliveryapp.Adapters;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.enge.deliveryapp.databinding.ListItemOrderLateBinding;
import com.enge.deliveryapp.databinding.ListItemOrderPreparedBinding;
import com.enge.deliveryapp.model.Order;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ListItemOrderDeliveredBinding;

import java.util.ArrayList;

public class OrdersListDeliveryboyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_DELIVERED = 1;
    private static final int TYPE_NOT_DELIVERED = 2;
    private static final int TYPE_LATE = 3;
    private static final int TYPE_NOT_ACCEPT = 4;

    private static final String DELIVERED = "Delivered";
    private static final String NOT_DELIVERED = "NotDelivered";
    private static final String LATE = "lated";
    private static final String NOT_ACCEPT = "notAccepted";

    private Context mContext;

    private ArrayList<Order> deliverdOrders;
    private ArrayList<Order> notDeliverdOrders;
    private ArrayList<Order> lateOrders;
    private ArrayList<Order> notAcceptedOrder;

    private OrdersDeliveryboyAdapterListener listener;

    public OrdersListDeliveryboyAdapter(Context mContext,
                                        ArrayList<Order> deliverdOrders,
                                        ArrayList<Order> notDeliverdOrders,
                                        ArrayList<Order> lateOrders,
                                        ArrayList<Order> notAcceptedOrder,
                                        OrdersDeliveryboyAdapterListener listener) {
        this.mContext = mContext;
        this.deliverdOrders = deliverdOrders;
        this.notDeliverdOrders = notDeliverdOrders;
        this.lateOrders = lateOrders;
        this.notAcceptedOrder = notAcceptedOrder;

        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return deliverdOrders.size() +
                notDeliverdOrders.size() +
                lateOrders.size() +
                notAcceptedOrder.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position < notAcceptedOrder.size()) {
            return TYPE_NOT_ACCEPT;
        } else if (position - notAcceptedOrder.size() < lateOrders.size()) {
            return TYPE_LATE;
        } else if (position - (notAcceptedOrder.size() + lateOrders.size()) < notDeliverdOrders.size()) {
            return TYPE_NOT_DELIVERED;

        } else {
            return TYPE_DELIVERED;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == TYPE_DELIVERED) {

            ListItemOrderDeliveredBinding deliveredBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.list_item_order_delivered, parent, false);

            return new deliveredViewHolder(deliveredBinding);

        } else if (viewType == TYPE_NOT_DELIVERED || viewType == TYPE_NOT_ACCEPT) {
            ListItemOrderPreparedBinding preparedBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.list_item_order_prepared, parent, false);

            return new preparedViewHolder(preparedBinding);

        } else if (viewType == TYPE_LATE) {
            ListItemOrderLateBinding lateBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.list_item_order_late, parent, false);
            return new lateViewHolder(lateBinding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Order order;

        switch (holder.getItemViewType()) {
            case TYPE_NOT_ACCEPT:
                order = notAcceptedOrder.get(position);
                initLayoutPrepared((preparedViewHolder) holder, order, position);
                break;
            case TYPE_LATE:
                order = lateOrders.get(position - notAcceptedOrder.size());
                initLayoutLate((lateViewHolder) holder, order, position);
                break;
            case TYPE_NOT_DELIVERED:
                order = notDeliverdOrders.get(position -
                        (notAcceptedOrder.size() + lateOrders.size()));
                initLayoutPrepared((preparedViewHolder) holder, order, position);
                break;

            case TYPE_DELIVERED:
                order = deliverdOrders.get(position -
                        (notAcceptedOrder.size() + lateOrders.size() + notDeliverdOrders.size()));

                initLayoutDelivered((deliveredViewHolder) holder, order, position);
                break;

            default:
                break;
        }
    }

    private void initLayoutLate(lateViewHolder holder, final Order order, final int position) {


        holder.binding.setModel(order);

        //round distance to two decimal places
        String distance = order.getDistanceFormatted() + mContext.getResources().getString(R.string.km);
        holder.binding.distance.setText(distance);

        //order PickTime formatted
        String orderPickTime = order.getOrderPickTimeFormatted();
        holder.binding.tvPickUpTime.setText(orderPickTime);

        // restaurant name formatted
        String restaurant_name = "[" + order.getDeliveryName() + "] " + order.getRestaurantName();
        holder.binding.restaurantName.setText(restaurant_name);

        holder.binding.listLate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                listener.onOrderClickedDeliveryboy(order, position, LATE);
            }
        });
    }

    private void initLayoutPrepared(final preparedViewHolder holder, final Order order, final int position) {

        holder.binding.setModel(order);

        //round distance to two decimal places
        String distance = order.getDistanceFormatted() + mContext.getResources().getString(R.string.km);
        holder.binding.distance.setText(distance);

        //order PickTime formatted
        String orderPickTime = order.getOrderPickTimeFormatted();
        holder.binding.tvPickUpTime.setText(orderPickTime);

        //restaurant name formatted
        if (holder.getItemViewType() == TYPE_NOT_DELIVERED) {
            String restaurant_name = "[" + order.getDeliveryName() + "] " + order.getRestaurantName();
            holder.binding.restaurantName.setText(restaurant_name);
        } else {
            holder.binding.restaurantName.setText(order.getRestaurantName());
        }

        if (holder.getItemViewType() == TYPE_NOT_ACCEPT) {
            Drawable drawable = holder.binding.listPrepared.getBackground();
            ColorDrawable colorDrawable = (ColorDrawable) drawable;

            ValueAnimator anim = new ValueAnimator();
            anim.setIntValues(colorDrawable.getColor(),
                    mContext.getResources().getColor(R.color.red));
            anim.setEvaluator(new ArgbEvaluator());
            anim.setRepeatCount(ValueAnimator.INFINITE);
            anim.setRepeatMode(ValueAnimator.REVERSE);
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    holder.binding.listPrepared.setBackgroundColor((Integer) valueAnimator.getAnimatedValue());
                }
            });
            anim.setDuration(600);
            anim.start();
        }


        holder.binding.listPrepared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.getItemViewType() == TYPE_NOT_ACCEPT) {

                    listener.onOrderClickedDeliveryboy(order, position, NOT_ACCEPT);
                } else {
                    listener.onOrderClickedDeliveryboy(order, position, NOT_DELIVERED);
                }
            }
        });
    }

    private void initLayoutDelivered(deliveredViewHolder holder, final Order order, final int position) {

        holder.binding.setModel(order);

        //round distance to two decimal places
        String distance = order.getDistanceFormatted() + mContext.getResources().getString(R.string.km);
        holder.binding.distance.setText(distance);

        //restaurant name formatted
        String restaurant_name = "[" + order.getDeliveryName() + "] " + order.getRestaurantName();
        holder.binding.restaurantName.setText(restaurant_name);

        holder.binding.listDelivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onOrderClickedDeliveryboy(order, position, DELIVERED);
            }
        });

    }


    // Static inner class to initialize the views of prepare rows

    public interface OrdersDeliveryboyAdapterListener {
        void onOrderClickedDeliveryboy(Order order, int position, String orderType);
    }

    public static class deliveredViewHolder extends RecyclerView.ViewHolder {

        private final ListItemOrderDeliveredBinding binding;

        public deliveredViewHolder(@NonNull ListItemOrderDeliveredBinding view) {

            super(view.getRoot());
            this.binding = view;
        }

    }

    public static class preparedViewHolder extends RecyclerView.ViewHolder {
        private final ListItemOrderPreparedBinding binding;

        public preparedViewHolder(@NonNull ListItemOrderPreparedBinding view) {
            super(view.getRoot());
            this.binding = view;
        }

    }

    // Orders DeliveryBoy Adapter Listener interface.

    public static class lateViewHolder extends RecyclerView.ViewHolder {
        private final ListItemOrderLateBinding binding;

        public lateViewHolder(@NonNull ListItemOrderLateBinding view) {
            super(view.getRoot());
            this.binding = view;
        }

    }
}
