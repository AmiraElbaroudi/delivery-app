package com.enge.deliveryapp.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.enge.deliveryapp.Activities.MessageActivity;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ListItemUserBinding;
import com.enge.deliveryapp.model.Message;
import com.enge.deliveryapp.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class UsersListAdapter  extends RecyclerView.Adapter<UsersListAdapter.ViewHolder>{

    private Context context;
    private ArrayList<User> users;
    private boolean isChat;
    private String theLastMessage;
    private String time;

    public UsersListAdapter(Context context, ArrayList<User> users,boolean isChat) {
        this.context = context;
        this.users = users;
        this.isChat = isChat;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        ListItemUserBinding userBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.list_item_user, parent, false);

        return new UsersListAdapter.ViewHolder(userBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final User user = users.get(position);
        holder.binding.txtUsername.setText(user.getUsername());
        holder.binding.listUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.binding.listUser.setBackgroundResource(R.color.darkgray);
                Intent intent = new Intent(context, MessageActivity.class);
                intent.putExtra("userId",user.getId());
                intent.putExtra("username",user.getUsername());
                context.startActivity(intent);
                ((Activity) context).finish();
            }
        });

        if (isChat){
            holder.binding.txtMessage.setVisibility(View.VISIBLE);
            holder.binding.txtTime.setVisibility(View.VISIBLE);
            lastMessage(user.getId(),holder.binding.txtMessage, holder.binding.txtTime);
        } else {
            holder.binding.txtMessage.setVisibility(View.GONE);
            holder.binding.txtTime.setVisibility(View.GONE);

        }
    }


    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final ListItemUserBinding binding;

        public ViewHolder(@NonNull ListItemUserBinding view) {
            super(view.getRoot());
            this.binding = view;
        }
    }

    //check for last message
    private void lastMessage(final String userid, final TextView last_msg, final TextView time_msg){
        theLastMessage = "default";
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("messages");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Message message = snapshot.getValue(Message.class);
                    if (firebaseUser != null && message != null) {
                        if (message.getToId().equals(firebaseUser.getUid()) && message.getFromId().equals(userid) ||
                                message.getToId().equals(userid) && message.getFromId().equals(firebaseUser.getUid())) {
                            theLastMessage = message.getText();
                            time = message.formatTime();
                        }
                    }
                }

                switch (theLastMessage){
                    case  "default":
                        last_msg.setText("");
                        time_msg.setText("");
                        break;

                    default:
                        last_msg.setText(theLastMessage);
                        time_msg.setText(time);

                        break;
                }

                theLastMessage = "default";
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
