package com.enge.deliveryapp.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.enge.deliveryapp.Activities.HomeActivity;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ListItemDeliveryBoyBinding;
import com.enge.deliveryapp.model.DeliveryBoy;

import java.util.ArrayList;

public class DeliveryBoysListAdapter extends RecyclerView.Adapter<DeliveryBoysListAdapter.DeliveryBoysViewHolder> {

    public static final int TYPE_CAR = 1;
    public static final int TYPE_MOTOR = 2;

    private Context context;
    private ArrayList<DeliveryBoy> deliveryBoys;

    public DeliveryBoysListAdapter(Context context, ArrayList<DeliveryBoy> deliveryBoys) {
        this.context = context;
        this.deliveryBoys = deliveryBoys;
    }

    @NonNull
    @Override
    public DeliveryBoysViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        ListItemDeliveryBoyBinding carBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.list_item_delivery_boy, parent, false);

        return new DeliveryBoysViewHolder(carBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull DeliveryBoysViewHolder holder, int i) {

        holder.binding.setModel(deliveryBoys.get(i));

        int nOfOrder = deliveryBoys.get(i).getnOfOrder();
        if(nOfOrder==0)
            holder.binding.numOfDeliveryBoy.setBackgroundResource(R.drawable.circle_green);
        if(nOfOrder==1)
            holder.binding.numOfDeliveryBoy.setBackgroundResource(R.drawable.circle_blue);
        if(nOfOrder==2)
            holder.binding.numOfDeliveryBoy.setBackgroundResource(R.drawable.circle_yellow);
        if(nOfOrder>3)
            holder.binding.numOfDeliveryBoy.setBackgroundResource(R.drawable.circle_red);
    }

    @Override
    public int getItemCount() {
        return deliveryBoys.size();
    }

    public  class DeliveryBoysViewHolder extends RecyclerView.ViewHolder {
        private final ListItemDeliveryBoyBinding binding;

        public DeliveryBoysViewHolder(@NonNull ListItemDeliveryBoyBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
