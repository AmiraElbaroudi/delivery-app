package com.enge.deliveryapp.Adapters;


import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.enge.deliveryapp.Enums.DeliveryStatusEnum;
import com.enge.deliveryapp.databinding.ListItemOrderLateBinding;
import com.enge.deliveryapp.databinding.ListItemOrderPreparedBinding;
import com.enge.deliveryapp.model.Order;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ListItemOrderDeliveredBinding;

import java.util.ArrayList;
import java.util.List;

public class OrdersListDistributorAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_DELIVERED = 1;
    private static final int TYPE_PREPARED = 2;
    private static final int TYPE_LATE = 3;

    private static final String DELIVERED = "Delivered";
    private static final String PREPARED = "prepared";
    private static final String LATE = "lated";

    private Context mContext;
    private ArrayList<Order> deliverdOrders;
    private ArrayList<Order> preparedOrders;
    private ArrayList<Order> lateOrders;
    private OrdersDistributorAdapterListener listener;

    public OrdersListDistributorAdapter(Context mContext,
                                        ArrayList<Order> deliverdOrders,
                                        ArrayList<Order> preparedOrders,
                                        ArrayList<Order> lateOrders,
                                        OrdersDistributorAdapterListener listener) {
        this.mContext = mContext;
        this.deliverdOrders = deliverdOrders;
        this.preparedOrders = preparedOrders;
        this.lateOrders = lateOrders;
        this.listener = listener;
    }


    @Override
    public int getItemCount() {
        return deliverdOrders.size()
                + preparedOrders.size()
                + lateOrders.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (position < lateOrders.size()) {
            return TYPE_LATE;
        } else if ((position - lateOrders.size()) < preparedOrders.size()) {
            return TYPE_PREPARED;
        } else {
            return TYPE_DELIVERED;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == TYPE_DELIVERED) {

            ListItemOrderDeliveredBinding deliveredBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.list_item_order_delivered, parent, false);

            return new OrdersListDistributorAdapter.deliveredDistributorViewHolder(deliveredBinding);

        } else if (viewType == TYPE_PREPARED) {
            ListItemOrderPreparedBinding preparedBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.list_item_order_prepared, parent, false);

            return new OrdersListDistributorAdapter.preparedDistributorViewHolder(preparedBinding);

        } else if (viewType == TYPE_LATE) {
            ListItemOrderLateBinding lateBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.list_item_order_late, parent, false);
            return new OrdersListDistributorAdapter.lateDistributorViewHolder(lateBinding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Order order;
        switch (holder.getItemViewType()) {
            case TYPE_LATE:
                order = lateOrders.get(position );
                initLayoutLate((OrdersListDistributorAdapter.lateDistributorViewHolder) holder, order, position);
                break;

            case TYPE_PREPARED:
                order = preparedOrders.get(position - lateOrders.size());
                initLayoutPrepared((OrdersListDistributorAdapter.preparedDistributorViewHolder) holder, order, position);
                break;

            case TYPE_DELIVERED:
                order = deliverdOrders.get((position - (lateOrders.size() + preparedOrders.size())));
                initLayoutDelivered((OrdersListDistributorAdapter.deliveredDistributorViewHolder) holder, order, position);
                break;

            default:
                break;
        }
    }

    private void initLayoutLate(final OrdersListDistributorAdapter.lateDistributorViewHolder holder, final Order order, final int position) {

        holder.binding.setModel(order);

        //round distance to two decimal places
        String distance = order.getDistanceFormatted() + mContext.getResources().getString(R.string.km);
        holder.binding.distance.setText(distance);

        //order PickTime formatted
        String orderPickTime = order.getOrderPickTimeFormatted();
        holder.binding.tvPickUpTime.setText(orderPickTime);

        // restaurant name formatted
        if(order.getDeliveryStatus() == DeliveryStatusEnum.NOT_ACCEPTED.getCode()){
            holder.binding.restaurantName.setText(order.getRestaurantName());

        }else {
            String restaurant_name = "[" + order.getDeliveryName() + "] " +
                    order.getRestaurantName();
            holder.binding.restaurantName.setText(restaurant_name);
        }

        holder.binding.listLate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onOrderClickedDistributor(order, position, LATE);
            }
        });


    }

    private void initLayoutPrepared(final OrdersListDistributorAdapter.preparedDistributorViewHolder holder, final Order order, final int position) {

        holder.binding.setModel(order);

        // delivery status is 0 , set layout background color orange with red .
        if(order.getDeliveryStatus() == DeliveryStatusEnum.NOT_ACCEPTED.getCode()){
            Drawable drawable = holder.binding.listPrepared.getBackground();
            ColorDrawable colorDrawable = (ColorDrawable) drawable;

            ValueAnimator anim = new ValueAnimator();
            anim.setIntValues(colorDrawable.getColor(),
                    mContext.getResources().getColor(R.color.red));
            anim.setEvaluator(new ArgbEvaluator());
            anim.setRepeatCount(ValueAnimator.INFINITE);
            anim.setRepeatMode(ValueAnimator.REVERSE);
            anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    holder.binding.listPrepared.setBackgroundColor((Integer)valueAnimator.getAnimatedValue());
                }
            });
            anim.setDuration(600);
            anim.start();
        }

        //round distance to two decimal places
        String distance = order.getDistanceFormatted() + mContext.getResources().getString(R.string.km);
        holder.binding.distance.setText(distance);

        //order PickTime formatted
        String orderPickTime = order.getOrderPickTimeFormatted();
        holder.binding.tvPickUpTime.setText(orderPickTime);

        // restaurant name formatted
        if(order.getDeliveryStatus() == DeliveryStatusEnum.NOT_ACCEPTED.getCode()){
            holder.binding.restaurantName.setText(order.getRestaurantName());
        }else {
            String restaurant_name = "[" + order.getDeliveryName() + "] " +
                    order.getRestaurantName();
            holder.binding.restaurantName.setText(restaurant_name);
        }

        holder.binding.listPrepared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onOrderClickedDistributor(order, position, PREPARED);
            }
        });

    }

    private void initLayoutDelivered(OrdersListDistributorAdapter.deliveredDistributorViewHolder holder, final Order order, final int position) {

        holder.binding.setModel(order);

        // round distance to two decimal places
        String distance = order.getDistanceFormatted() + mContext.getResources().getString(R.string.km);
        holder.binding.distance.setText(distance);

        // restaurant name formatted
        String restaurant_name = "[" + order.getDeliveryName() + "] " +
                order.getRestaurantName();
        holder.binding.restaurantName.setText(restaurant_name);

        holder.binding.listDelivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onOrderClickedDistributor(order, position, DELIVERED);
            }
        });

    }

    // Static inner class to initialize the views of prepare rows

    public static class deliveredDistributorViewHolder extends RecyclerView.ViewHolder {

        private final ListItemOrderDeliveredBinding binding;

        public deliveredDistributorViewHolder(@NonNull ListItemOrderDeliveredBinding view) {

            super(view.getRoot());
            this.binding = view;
        }
    }

    public static class preparedDistributorViewHolder extends RecyclerView.ViewHolder {
        private final ListItemOrderPreparedBinding binding;

        public preparedDistributorViewHolder(@NonNull ListItemOrderPreparedBinding view) {
            super(view.getRoot());
            this.binding = view;
        }
    }

    public static class lateDistributorViewHolder extends RecyclerView.ViewHolder {
        private final ListItemOrderLateBinding binding;

        public lateDistributorViewHolder(@NonNull ListItemOrderLateBinding view) {
            super(view.getRoot());
            this.binding = view;
        }
    }

    // OrdersAdapter Listener interface.
    public interface OrdersDistributorAdapterListener {
        void onOrderClickedDistributor(Order order, int position, String orderType);
    }

}
