package com.enge.deliveryapp.Adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.model.Message;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    public static int MSG_TYPE_LEFT = 0;
    public static int MSG_TYPE_RIGHT = 1;
    FirebaseUser firebaseDatabase;
    private Context context;
    private ArrayList<Message> messages;
    StorageReference storageReference;
//    OnBottomReachedListener onBottomReachedListener;

    public MessageAdapter(Context context, ArrayList<Message> messages) {
        this.context = context;
        this.messages = messages;
        storageReference = FirebaseStorage.getInstance().getReference();

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == MSG_TYPE_RIGHT) {
            View view = LayoutInflater.from(context).inflate(R.layout.message_item_right,
                    parent, false);

            return new MessageAdapter.ViewHolder(view);
        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.message_item_left,
                    parent, false);

            return new MessageAdapter.ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

//        if (position == messages.size() - 1){
//            onBottomReachedListener.onBottomReached(position);
//        }
        Message message = messages.get(position);

        holder.show_message.setText(message.getText());

        if(message.getImageUrl() != null){
            holder.bubbel_image.setVisibility(View.VISIBLE);

            Glide.with(context).load(message.getImageUrl()).into(holder.show_image);
            holder.show_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
        if(message.getVideoUrl() != null){
            Toast.makeText(context,"video",Toast.LENGTH_LONG).show();

            holder.bubbel_video.setVisibility(View.VISIBLE);
            holder.show_video.setVideoURI(Uri.parse(message.getVideoUrl()));

        }

        if(message.getText() == null || (message.getText().isEmpty())){
            holder.bubbel_text.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    @Override
    public int getItemViewType(int position) {
        firebaseDatabase = FirebaseAuth.getInstance().getCurrentUser();

        if (messages.get(position).getFromId().equals(firebaseDatabase.getUid())) {
            return MSG_TYPE_RIGHT;
        } else
            return MSG_TYPE_LEFT;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView show_message;
        public ImageView show_image;
        public VideoView show_video;
        public RelativeLayout bubbel_text;
        public RelativeLayout bubbel_image;
        public RelativeLayout bubbel_video;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            show_message = itemView.findViewById(R.id.show_message);
            show_image = itemView.findViewById(R.id.show_image);
            show_video = itemView.findViewById(R.id.show_video);

            bubbel_text = itemView.findViewById(R.id.bubbel_text);
            bubbel_image = itemView.findViewById(R.id.bubbel_image);
            bubbel_video = itemView.findViewById(R.id.bubbel_video);

            MediaController mediaControls = new MediaController(context);
            mediaControls.setAnchorView( show_video);
            show_video.requestFocus();

            show_video.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!show_video.isPlaying()){
                        show_video.start();
                    }
                    else{
                        show_video.pause();
                    }

                }
            });


}

        }

    }
//    public interface OnBottomReachedListener {
//
//        void onBottomReached(int position);
//
//    }
//    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener){
//
//        this.onBottomReachedListener = onBottomReachedListener;
//    }

