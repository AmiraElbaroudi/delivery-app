package com.enge.deliveryapp.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.enge.deliveryapp.Enums.DeliveryStatusEnum;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ListItemOrderItemBinding;
import com.enge.deliveryapp.model.Mail;

import java.util.ArrayList;

public class OrderItemsListAdapter extends RecyclerView.Adapter<OrderItemsListAdapter.OrderItemViewHolder>{

    Context context;
    ArrayList<Mail> mails;
    String order_type;
int delivery_staus;
    public OrderItemsListAdapter(Context context, ArrayList<Mail> mails, String order_type, int deliveryStatus) {
        this.context = context;
        this.mails = mails;
        this.order_type = order_type;
        this.delivery_staus = delivery_staus;
    }

    @NonNull
    @Override
    public OrderItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        ListItemOrderItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.list_item_order_item, parent, false);

        return new OrderItemViewHolder(binding);    }

    @Override
    public void onBindViewHolder(@NonNull OrderItemViewHolder holder, int i) {
        holder.binding.setModel(mails.get(i));
        if(order_type.equals("Delivered") ||
                (order_type.equals("Late") && delivery_staus == DeliveryStatusEnum.DELIVRE.getCode())){
            holder.binding.rbChecked.setVisibility(View.INVISIBLE);
        }}


    @Override
    public int getItemCount() {
        return mails.size();
    }

    public class OrderItemViewHolder extends RecyclerView.ViewHolder {
        private final ListItemOrderItemBinding binding;

        public OrderItemViewHolder(@NonNull ListItemOrderItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
