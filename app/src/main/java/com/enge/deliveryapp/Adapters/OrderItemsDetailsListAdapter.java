package com.enge.deliveryapp.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ListItemOrderItemDetailsBinding;
import com.enge.deliveryapp.model.OrderItem;

import java.util.ArrayList;
import java.util.List;

public class OrderItemsDetailsListAdapter extends RecyclerView.Adapter<OrderItemsDetailsListAdapter.MyViewHolder> {

    private static List<OrderItem> data;
    private Context mContext;


    public OrderItemsDetailsListAdapter(Context mContext, ArrayList<OrderItem> data) {
        this.mContext = mContext;
        this.data = data;

    }
    @NonNull
    @Override
    public OrderItemsDetailsListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        ListItemOrderItemDetailsBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.list_item_order_item_details, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderItemsDetailsListAdapter.MyViewHolder holder, final int position) {

        holder.binding.setModel(data.get(position));

        String variantStr="";
        String accessories="";

        ArrayList<OrderItem.OrderExtras> extras=data.get(position).getExtras();
        ArrayList<OrderItem.OrderEspecials> especialies=data.get(position).getEspecials();


        if(extras!=null){
            for(int i=0;i<extras.size();i++){
                if(i!=0)
                    accessories=accessories+",";
                accessories=accessories+extras.get(i).getExtrasName();

            }
        }

        if(especialies!=null){
            for(int i=0;i<especialies.size();i++){
                if(i!=0)
                    variantStr=variantStr+",";
                variantStr=variantStr+especialies.get(i).getEspecialName();

            }
        }
        holder.binding.accessoriesTextView.setText(accessories);
        holder.binding.variantTextView.setText(variantStr);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ListItemOrderItemDetailsBinding binding;

        public MyViewHolder(@NonNull ListItemOrderItemDetailsBinding view) {
            super(view.getRoot());
            this.binding = view;
        }
    }



}
