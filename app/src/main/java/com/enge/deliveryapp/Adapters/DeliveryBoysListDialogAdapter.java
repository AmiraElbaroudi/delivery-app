package com.enge.deliveryapp.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ListItemDeliveryBoyBinding;
import com.enge.deliveryapp.databinding.ListItemDeliveryBoyDialogBinding;
import com.enge.deliveryapp.model.DeliveryBoy;

import java.util.ArrayList;

public class DeliveryBoysListDialogAdapter extends RecyclerView.Adapter<DeliveryBoysListDialogAdapter.DeliveryBoysDialogViewHolder>{
    private Context context;
    private ArrayList<DeliveryBoy> deliveryBoys;
    private  int mSelectedItem = -1;

    public DeliveryBoysListDialogAdapter(Context context, ArrayList<DeliveryBoy> deliveryBoys) {
        this.context = context;
        this.deliveryBoys = deliveryBoys;
    }

    @NonNull
    @Override
    public DeliveryBoysDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        ListItemDeliveryBoyDialogBinding dialogBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.list_item_delivery_boy_dialog, parent, false);

        return new DeliveryBoysDialogViewHolder(dialogBinding);    }

    @Override
    public void onBindViewHolder(@NonNull DeliveryBoysDialogViewHolder holder, int position) {


        holder.binding.setModel(deliveryBoys.get(position));
        if (position == 0){
            holder.binding.numOfDeliveryBoy.setVisibility(View.INVISIBLE);
        }

        int nOfOrder = deliveryBoys.get(position).getnOfOrder();
        if(nOfOrder==0)
            holder.binding.numOfDeliveryBoy.setBackgroundResource(R.drawable.circle_green);
        if(nOfOrder==1)
            holder.binding.numOfDeliveryBoy.setBackgroundResource(R.drawable.circle_blue);
        if(nOfOrder==2)
            holder.binding.numOfDeliveryBoy.setBackgroundResource(R.drawable.circle_yellow);
        if(nOfOrder>3)
            holder.binding.numOfDeliveryBoy.setBackgroundResource(R.drawable.circle_red);

        //since only one radio button is allowed to be selected,
        // this condition un-checks previous selections
        //check the radio button if both position and selectedPosition matches
        holder.binding.radio.setChecked(position == mSelectedItem);


    }

    @Override
    public int getItemCount() {
        return deliveryBoys.size();
    }

    //Return the selectedPosition item
    public DeliveryBoy getSelectedItem() {
        if (mSelectedItem != -1) {
            return deliveryBoys.get(mSelectedItem);
        }
        return null;
    }

    public class DeliveryBoysDialogViewHolder extends RecyclerView.ViewHolder {
        private final ListItemDeliveryBoyDialogBinding binding;

        public DeliveryBoysDialogViewHolder(@NonNull ListItemDeliveryBoyDialogBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.radio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedItem = getAdapterPosition();
                    notifyDataSetChanged();

                    //TODO this in case more delivery boy in list
                    //notifyItemRangeChanged(0, deliveryBoys.size()); //blink list problem

//                    int copyOfSelectedItem = mSelectedItem;
//                    mSelectedItem = getAdapterPosition();
//                    notifyItemChanged(copyOfSelectedItem);
//                    notifyItemChanged(mSelectedItem);

                }
            });
        }
    }

}
