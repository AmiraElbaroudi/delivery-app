package com.enge.deliveryapp.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ListItemBillBinding;
import com.enge.deliveryapp.model.OrderItem;

import java.util.ArrayList;
import java.util.List;

public class PrintItemListAdapter extends RecyclerView.Adapter<PrintItemListAdapter.MyViewHolder>{

    private List<OrderItem> orderItems;
    private Context mContext;

    public PrintItemListAdapter(Context mContext, List<OrderItem> orderItems) {
        this.mContext = mContext;
        this.orderItems = orderItems;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        ListItemBillBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.list_item_bill, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.binding.setModel(orderItems.get(position));

        String variantStr="";
        String accessories="";

        ArrayList<OrderItem.OrderExtras> extras= orderItems.get(position).getExtras();
        ArrayList<OrderItem.OrderEspecials> especialies= orderItems.get(position).getEspecials();


        if(especialies!=null){
            for(int i=0;i<especialies.size();i++){
                if(i==0)
                    variantStr = "Variant: \u0020";
                else
                    variantStr = variantStr + "\nVariant: \u0020";

                variantStr = variantStr+especialies.get(i).getEspecialName();
            }
        }

        if(extras!=null){
            for(int i=0;i<extras.size();i++){
                if(i==0)
                    accessories="+ \u0020";
                else
                    accessories= accessories + "\n+ \u0020";
                accessories=accessories+extras.get(i).getExtrasName();
            }
        }

        holder.binding.txtVariant.setText(variantStr);
        holder.binding.txtAccessories.setText(accessories);
    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ListItemBillBinding binding;

        public MyViewHolder(@NonNull ListItemBillBinding listItemBillBinding) {
            super(listItemBillBinding.getRoot());
            this.binding = listItemBillBinding;
        }
    }
}
