package com.enge.deliveryapp.Enums;

import com.enge.deliveryapp.Helpers.Utils;
import com.enge.deliveryapp.MyApplication;


public enum DeliveryStatusEnum {

// the reason why the late order is late.
    NOT_ACCEPTED(0, "notAccepted"),//Delivery boy declined the request
    ACCEPT_DELIVERY(1, "acceptDelivery"),//Delivery boy accepted the request.
    DELIVERY_ARRIVE_RESTAURANT(2, "deliveryArriveRestaurant"),//Delivery boy arrived restaurant.
    TAKEN(3, "taken"),//Delivery boy received the order from the restaurant.
   DELIVRE(4, "deliver");// Delivery boy delivered the order to the customer.



    private int code;
    private String value;

     DeliveryStatusEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public static String getValue(int code) {
        DeliveryStatusEnum[] types = values();
        for (int i = 0; i < types.length; i++) {
            DeliveryStatusEnum type = types[i];
            if (code == type.getCode())
                return type.getValue();
        }

        return "";
    }

    public static int getCode(String value) {
        DeliveryStatusEnum[] types = values();
        for (int i = 0; i < types.length; i++) {
            DeliveryStatusEnum type = types[i];
            if (value.equalsIgnoreCase(type.getValue()))
                return type.getCode();
        }

        return 0;
    }

    public int getCode() {
        return code;
    }

    public String getValue() {

        return Utils.getStringResourceByName(MyApplication.getApplication() ,value);


    }

}
