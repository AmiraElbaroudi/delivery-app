package com.enge.deliveryapp.Enums;

import com.enge.deliveryapp.Helpers.Utils;
import com.enge.deliveryapp.MyApplication;

public enum PreparationStatusEnum {
    PENDING(0, "pending"),
    UNDER_PREPARATION(1, "underPreparation"),
    PREPARED(2, "prepared"),
    SEND_ORDER_FOR_DELIEVERY(3, "sendOrderForDelivery"),
    ACCEPT_DELIVERY(4, "acceptDelivery"),
    REFUSED_DELIVERY(5, "refusedDelivery"),
    DELIVERY_ARRIVE_RESTAURANT(6, "deliveryArriveRestaurant"),
    TAKEN(7, "taken"),
    DELIVER(8, "deliver"),
    NOT_DELIVER(9, "notdeliver"),
    CANCELED(10, "canceled");


    private int code;
    private String value;

     PreparationStatusEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }

    public static String getValue(int code) {
        PreparationStatusEnum[] types = values();
        for (int i = 0; i < types.length; i++) {
            PreparationStatusEnum type = types[i];
            if (code == type.getCode())
                return type.getValue();
        }

        return "";
    }

    public static int getCode(String value) {
        PreparationStatusEnum[] types = values();
        for (int i = 0; i < types.length; i++) {
            PreparationStatusEnum type = types[i];
            if (value.equalsIgnoreCase(type.getValue()))
                return type.getCode();
        }

        return 0;
    }

    public int getCode() {
        return code;
    }

    public String getValue() {

        return Utils.getStringResourceByName(MyApplication.getApplication() ,value);


    }

}
