package com.enge.deliveryapp.Enums;

import com.enge.deliveryapp.Helpers.Utils;
import com.enge.deliveryapp.MyApplication;

public enum RoleNameEnum {

    DISTRIBUTOR(0, "Distributor"),
    MOTOR(1, "Motor"),
    CAR(2, "Car");

    private int code;
    private String value;

    RoleNameEnum(int code, String value) {
        this.code = code;
        this.value = value;
    }
    public static String getValue(int code) {
        RoleNameEnum[] types = values();
        for (int i = 0; i < types.length; i++) {
            RoleNameEnum type = types[i];
            if (code == type.getCode())
                return type.getValue();
        }

        return "";
    }

    public static int getCode(String value) {
        RoleNameEnum[] types = values();
        for (int i = 0; i < types.length; i++) {
            RoleNameEnum type = types[i];
            if (value.equalsIgnoreCase(type.getValue()))
                return type.getCode();
        }

        return 0;
    }

    public int getCode() {
        return code;
    }

    public String getValue() {

        return Utils.getStringResourceByName(MyApplication.getApplication() ,value);

    }

}
