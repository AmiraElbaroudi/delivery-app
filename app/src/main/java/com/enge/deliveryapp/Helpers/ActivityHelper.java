package com.enge.deliveryapp.Helpers;
import android.content.Context;
import android.content.Intent;

import com.enge.deliveryapp.Activities.LoginActivity;


public class ActivityHelper {

    public static void goToLoginActivity(Context context) {
        Intent i = new Intent(context, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(i);
    }
}