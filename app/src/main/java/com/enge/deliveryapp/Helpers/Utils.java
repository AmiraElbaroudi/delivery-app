package com.enge.deliveryapp.Helpers;

import android.content.Context;

import com.google.firebase.crash.FirebaseCrash;

public class Utils {

    public static String getStringResourceByName(Context context, String name){

        try {
            String packageName = context.getPackageName();
            int resId = context.getResources().getIdentifier(name, "string", packageName);
            return context.getString(resId);
        }catch (Exception e){
            FirebaseCrash.report(e);

            return  "";
        }
    }
}
