package com.enge.deliveryapp.model;



import com.enge.deliveryapp.Enums.PreparationStatusEnum;

import java.io.Serializable;

public class OrderDataDetails implements Serializable{

    private int orderId,orderDeliveryType,preparationStatus;

    private String orderDeliveryTime,paymentMethods,preparationOrderTime,address
            ,userComments,creationDate;

    private boolean isNow;

    private double totalPrice,priceAfterDiscount,finalPrice;


    public OrderDataDetails(){

    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getOrderDeliveryType() {
        return orderDeliveryType;
    }

    public void setOrderDeliveryType(int orderDeliveryType) {
        this.orderDeliveryType = orderDeliveryType;
    }

    public int getPreparationStatus() {
        return preparationStatus;
    }

    public void setPreparationStatus(int preparationStatus) {
        this.preparationStatus = preparationStatus;
    }

    public String getOrderDeliveryTime() {
        return orderDeliveryTime;
    }

    public void setOrderDeliveryTime(String orderDeliveryTime) {
        this.orderDeliveryTime = orderDeliveryTime;
    }

    public String getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(String paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public String getPreparationOrderTime() {
        return preparationOrderTime;
    }

    public void setPreparationOrderTime(String preparationOrderTime) {
        this.preparationOrderTime = preparationOrderTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserComments() {
        return userComments;
    }

    public void setUserComments(String userComments) {
        this.userComments = userComments;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isNow() {
        return isNow;
    }

    public void setNow(boolean now) {
        isNow = now;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getPriceAfterDiscount() {
        return priceAfterDiscount;
    }

    public void setPriceAfterDiscount(double priceAfterDiscount) {
        this.priceAfterDiscount = priceAfterDiscount;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getPreprationStatusStr(){
        return PreparationStatusEnum.getValue(preparationStatus);
    }

    public String getDeliveryTypeStr(){
        return PreparationStatusEnum.getValue(orderDeliveryType);
    }
}
