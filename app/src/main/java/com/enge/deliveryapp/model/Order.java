package com.enge.deliveryapp.model;

import android.annotation.SuppressLint;

import com.google.firebase.crash.FirebaseCrash;

import java.io.Serializable;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class Order implements Serializable{
    private int orderId;
    private String restaurantName;
    private String deliveryRepresentedLetter;
    private String deliveryName;
    private double distance;
    private int preparationStatus;
    private int deliveryStatus;
    private String orderPickTime;
    private String orderArriveTime;
    private String preparationOrderTime;
    private String orderDeliverdTimeToDeliverBoy;

    public Order() {
    }

    public Order(int orderId, String restaurantName, int preparationStatus) {
        this.orderId = orderId;
        this.restaurantName = restaurantName;
        this.preparationStatus = preparationStatus;
    }


    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getDeliveryRepresentedLetter() {
        return deliveryRepresentedLetter;
    }

    public void setDeliveryRepresentedLetter(String deliveryRepresentedLetter) {
        this.deliveryRepresentedLetter = deliveryRepresentedLetter;
    }

    public String getDeliveryName() {
        return deliveryName;
    }

    public void setDeliveryName(String deliveryName) {
        this.deliveryName = deliveryName;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getPreparationStatus() {
        return preparationStatus;
    }

    public void setPreparationStatus(int preparationStatus) {
        this.preparationStatus = preparationStatus;
    }

    public int getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(int deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getOrderPickTime() {
        return orderPickTime;
    }

    public void setOrderPickTime(String orderPickTime) {
        this.orderPickTime = orderPickTime;
    }

    public String getOrderArriveTime() {
        return orderArriveTime;
    }

    public void setOrderArriveTime(String orderArriveTime) {
        this.orderArriveTime = orderArriveTime;
    }

    public String getPreparationOrderTime() {
        return preparationOrderTime;
    }

    public void setPreparationOrderTime(String preparationOrderTime) {
        this.preparationOrderTime = preparationOrderTime;
    }

    public String getOrderDeliverdTimeToDeliverBoy() {
        return orderDeliverdTimeToDeliverBoy;
    }

    public void setOrderDeliverdTimeToDeliverBoy(String orderDeliverdTimeToDeliverBoy) {
        this.orderDeliverdTimeToDeliverBoy = orderDeliverdTimeToDeliverBoy;
    }


    public String getPreparationOrderTimeFormatted() {
        SimpleDateFormat outputDateFormat = new SimpleDateFormat("mm", Locale.US);
        SimpleDateFormat inputDateFormat = new SimpleDateFormat("HH:mm", Locale.US);

        Date date = null;
        String time = null;
        try {
            date = inputDateFormat.parse(preparationOrderTime);
            time = outputDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            FirebaseCrash.report(e);
        }

        return time;
    }

    public String getOrderPickTimeFormatted() {
        SimpleDateFormat outputDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);
        SimpleDateFormat inputDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);

        Date date = null;
        String time = null;
        try {
            time = orderPickTime.substring(0,7);
            date = inputDateFormat.parse(time);
            time = outputDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            FirebaseCrash.report(e);
        }
        return time;
    }

    public String getDistanceFormatted() {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.HALF_EVEN);

        return df.format(distance);
    }

    public String getOrderDeliverdTimeToDeliverBoy_Formatted() {
        if(orderDeliverdTimeToDeliverBoy==null||orderDeliverdTimeToDeliverBoy.length()==0)
            return "00:00";

        SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        SimpleDateFormat outputDateFormat = new SimpleDateFormat("HH:mm", Locale.US);

        Date date = null;
        String time = null;
        try {

            time = orderDeliverdTimeToDeliverBoy;
            date = inputDateFormat.parse(time);
            time = outputDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            FirebaseCrash.report(e);

        }

        return time;
    }
}
