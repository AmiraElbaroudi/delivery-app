package com.enge.deliveryapp.model;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderDetails implements Serializable {

    private OrderDataDetails orderDetails;

    private UserDataDetails userData;

    private String timeRemainingToPrePareOrder;

    private ArrayList<OrderItem> items = new ArrayList<>();


    public OrderDetails() {
    }

    public OrderDataDetails getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(OrderDataDetails orderDetails) {
        this.orderDetails = orderDetails;
    }

    public UserDataDetails getUserData() {
        return userData;
    }

    public void setUserData(UserDataDetails userData) {
        this.userData = userData;
    }

    public String getTimeRemainingToPrePareOrder() {
        return timeRemainingToPrePareOrder;
    }

    public void setTimeRemainingToPrePareOrder(String timeRemainingToPrePareOrder) {
        this.timeRemainingToPrePareOrder = timeRemainingToPrePareOrder;
    }

    public ArrayList<OrderItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<OrderItem> items) {
        this.items = items;
    }
}
