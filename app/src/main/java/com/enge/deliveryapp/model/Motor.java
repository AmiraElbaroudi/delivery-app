package com.enge.deliveryapp.model;

import java.io.Serializable;

public class Motor  implements Serializable {

    private int motorId;
    private String name;
    private String motorNo;
    private int nOfOrder;

    public Motor() {
    }

    public Motor(String name) {
        this.name = name;
    }

    public int getMotorId() {
        return motorId;
    }

    public void setMotorId(int motorId) {
        this.motorId = motorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMotorNo() {
        return motorNo;
    }

    public void setMotorNo(String motorNo) {
        this.motorNo = motorNo;
    }

    public int getnOfOrder() {
        return nOfOrder;
    }

    public void setnOfOrder(int nOfOrder) {
        this.nOfOrder = nOfOrder;
    }
}
