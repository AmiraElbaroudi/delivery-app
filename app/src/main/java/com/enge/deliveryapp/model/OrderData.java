package com.enge.deliveryapp.model;

import java.io.Serializable;

public class OrderData implements Serializable {

    private RestaurantData restaurant;
    private UserData user ;
    private UserCoord userCoord;

    public OrderData() {
    }

    public RestaurantData getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(RestaurantData restaurantData) {
        this.restaurant = restaurantData;
    }

    public UserData getUser() {
        return user;
    }

    public void setUserData(UserData userData) {
        this.user = userData;
    }

    public UserCoord getUserCoord() {
        return userCoord;
    }

    public void setUserCoord(UserCoord userCoord) {
        this.userCoord = userCoord;
    }
}
