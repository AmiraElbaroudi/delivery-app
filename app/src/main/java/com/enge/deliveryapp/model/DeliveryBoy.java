package com.enge.deliveryapp.model;

import java.io.Serializable;

public class DeliveryBoy implements Serializable {
    private int deliveryBoyId;
    private String name;
    private String deliveryBoyNo;
    private int nOfOrder;

    public DeliveryBoy() {
    }

    public DeliveryBoy(int deliveryBoyId, String name, String deliveryBoyNo, int nOfOrder) {
        this.deliveryBoyId = deliveryBoyId;
        this.name = name;
        this.deliveryBoyNo = deliveryBoyNo;
        this.nOfOrder = nOfOrder;
    }

    public DeliveryBoy(String name) {
        this.name = name;
    }

    public int getDeliveryBoyId() {
        return deliveryBoyId;
    }

    public void setDeliveryBoyId(int deliveryBoyId) {
        this.deliveryBoyId = deliveryBoyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeliveryBoyNo() {
        return deliveryBoyNo;
    }

    public void setDeliveryBoyNo(String deliveryBoyNo) {
        this.deliveryBoyNo = deliveryBoyNo;
    }

    public int getnOfOrder() {
        return nOfOrder;
    }

    public void setnOfOrder(int nOfOrder) {
        this.nOfOrder = nOfOrder;
    }
}
