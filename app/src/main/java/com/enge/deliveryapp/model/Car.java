package com.enge.deliveryapp.model;

import java.io.Serializable;

public class Car implements Serializable {

    private int carId;
    private String name;
    private String carNo;
    private int nOfOrder;

    public Car() {
    }

    public Car(String name) {
        this.name = name;
    }

    public int getCarId() {
        return carId;
    }

    public void setCarId(int carId) {
        this.carId = carId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo;
    }

    public int getnOfOrder() {
        return nOfOrder;
    }

    public void setnOfOrder(int nOfOrder) {
        this.nOfOrder = nOfOrder;
    }
}
