package com.enge.deliveryapp.model;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderDistributor implements Serializable {

    private ArrayList<Order>  deliverdOrders;
    private ArrayList<Order>  preparedOrders;
    private ArrayList<Order>  lateOrders;
    private String time;

    public OrderDistributor() {
    }

    public ArrayList<Order> getDeliverdOrders() {
        return deliverdOrders;
    }

    public void setDeliverdOrders(ArrayList<Order> deliverdOrders) {
        this.deliverdOrders = deliverdOrders;
    }

    public ArrayList<Order> getPreparedOrders() {
        return preparedOrders;
    }

    public void setPreparedOrders(ArrayList<Order> preparedOrders) {
        this.preparedOrders = preparedOrders;
    }

    public ArrayList<Order> getLateOrders() {
        return lateOrders;
    }

    public void setLateOrders(ArrayList<Order> lateOrders) {
        this.lateOrders = lateOrders;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
