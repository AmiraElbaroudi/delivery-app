package com.enge.deliveryapp.model;

import java.io.Serializable;

public class Mail implements Serializable {

    private String mailName;
    private int mailNo;

    public Mail() {
    }

    public String getMailName() {
        return mailName;
    }

    public void setMailName(String mailName) {
        this.mailName = mailName;
    }

    public int getMailNo() {
        return mailNo;
    }

    public void setMailNo(int mailNo) {
        this.mailNo = mailNo;
    }
}
