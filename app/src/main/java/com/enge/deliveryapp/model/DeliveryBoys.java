package com.enge.deliveryapp.model;

import java.io.Serializable;
import java.util.ArrayList;

public class DeliveryBoys implements Serializable {

    private ArrayList<Car> cars = new ArrayList<>();
    private ArrayList<Motor> motors = new ArrayList<>();

    public DeliveryBoys() {
    }

    public ArrayList<Car> getCars() {
        return cars;
    }
    public void setCars(ArrayList<Car> cars) {
        this.cars = cars;
    }

    public ArrayList<Motor> getMotors() {
        return motors;
    }
    public void setMotors(ArrayList<Motor> motors) {
        this.motors = motors;
    }


}
