package com.enge.deliveryapp.model;

import com.google.firebase.crash.FirebaseCrash;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class RestaurantData implements Serializable {
    private int orderId;
    private String restaurantName;
    private String ordertime;
    private String restaurantAddress;
    private String restLat;
    private String restLng;
    private String restaurantPhone;
    private ArrayList<Mail> mails = new ArrayList<>();
    private String orderPickTime;
    private String orderShouldArriveTime;

    public RestaurantData() {
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(String ordertime) {
        this.ordertime = ordertime;
    }

    public String getRestaurantAddress() {
        return restaurantAddress;
    }

    public void setRestaurantAddress(String restaurantAddress) {
        this.restaurantAddress = restaurantAddress;
    }

    public String getRestLat() {
        return restLat;
    }

    public void setRestLat(String restLat) {
        this.restLat = restLat;
    }

    public String getRestLng() {
        return restLng;
    }

    public void setRestLng(String restLng) {
        this.restLng = restLng;
    }

    public String getRestaurantPhone() {
        return restaurantPhone;
    }

    public void setRestaurantPhone(String restaurantPhone) {
        this.restaurantPhone = restaurantPhone;
    }

    public ArrayList<Mail> getMails() {
        return mails;
    }

    public void setMails(ArrayList<Mail> mails) {
        this.mails = mails;
    }

    public String getOrderPickTime() {
        return orderPickTime;
    }

    public void setOrderPickTime(String orderPickTime) {
        this.orderPickTime = orderPickTime;
    }

    public String getOrderShouldArriveTime() {
        return orderShouldArriveTime;
    }

    public void setOrderShouldArriveTime(String orderShouldArriveTime) {
        this.orderShouldArriveTime = orderShouldArriveTime;
    }


    public String getTimeFormatted(String t) {
        SimpleDateFormat outputDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);
        SimpleDateFormat inputDateFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);

        Date date = null;
        String time = null;
        try {
            time = t.substring(0,7);
            date = inputDateFormat.parse(time);
            time = outputDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            FirebaseCrash.report(e);
        }

        return time;
    }

}
