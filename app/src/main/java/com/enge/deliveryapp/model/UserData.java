package com.enge.deliveryapp.model;

import java.io.Serializable;

public class UserData implements Serializable {

    private String userName;
    private String userAddress;
    private String userPhone;
    private String userEmail;
    private int userDoor;
    private int userFloor;
    private String userCode;

    public UserData() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public int getUserDoor() {
        return userDoor;
    }

    public void setUserDoor(int userDoor) {
        this.userDoor = userDoor;
    }

    public int getUserFloor() {
        return userFloor;
    }

    public void setUserFloor(int userFloor) {
        this.userFloor = userFloor;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }
}
