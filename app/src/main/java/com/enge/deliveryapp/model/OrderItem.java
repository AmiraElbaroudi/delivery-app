package com.enge.deliveryapp.model;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderItem implements Serializable {

    private int itemId, quentity;

    private String itemName;

    private double itemPrice;

    private ArrayList<OrderExtras>extras = new ArrayList<>();

    private ArrayList<OrderEspecials>especials = new ArrayList<>();


    public OrderItem(){

    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getQuentity() {
        return quentity;
    }

    public void setQuentity(int quentity) {
        this.quentity = quentity;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public ArrayList<OrderExtras> getExtras() {
        return extras;
    }

    public void setExtras(ArrayList<OrderExtras> extras) {
        this.extras = extras;
    }

    public ArrayList<OrderEspecials> getEspecials() {
        return especials;
    }

    public void setEspecials(ArrayList<OrderEspecials> especials) {
        this.especials = especials;
    }

    public class OrderEspecials implements Serializable{

        int especialsId;
        String especialName;
        double especialPrice;

        public OrderEspecials(){

        }

        public int getEspecialsId() {
            return especialsId;
        }

        public void setEspecialsId(int especialsId) {
            this.especialsId = especialsId;
        }

        public String getEspecialName() {
            return especialName;
        }

        public void setEspecialName(String especialName) {
            this.especialName = especialName;
        }

        public double getEspecialPrice() {
            return especialPrice;
        }

        public void setEspecialPrice(double especialPrice) {
            this.especialPrice = especialPrice;
        }
    }


    public class OrderExtras implements Serializable{

        int extrasId;
        String extrasName;
        double extrasPrice;

        public OrderExtras(){

        }

        public int getExtrasId() {
            return extrasId;
        }

        public void setExtrasId(int extrasId) {
            this.extrasId = extrasId;
        }

        public String getExtrasName() {
            return extrasName;
        }

        public void setExtrasName(String extrasName) {
            this.extrasName = extrasName;
        }

        public double getExtrasPrice() {
            return extrasPrice;
        }

        public void setExtrasPrice(double extrasPrice) {
            this.extrasPrice = extrasPrice;
        }
    }

}