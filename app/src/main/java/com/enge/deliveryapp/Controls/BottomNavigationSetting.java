package com.enge.deliveryapp.Controls;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.BottomNavigationView;
import android.view.View;

import com.enge.deliveryapp.Activities.ChatActivity;
import com.enge.deliveryapp.Activities.HomeActivity;
import com.enge.deliveryapp.R;

public class BottomNavigationSetting {

    public static void enableNavigation(final Context context, final Activity callingActivity, View view){

        switch (view.getId()){

            case R.id.btn_chat:
                if( callingActivity instanceof HomeActivity){
                    Intent intent1 = new Intent(context, ChatActivity.class);
                    context.startActivity(intent1);
                    intent1.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                }

                break;

            case R.id.btn_order:
                if(callingActivity instanceof ChatActivity) {
                    Intent intent2 = new Intent(context, HomeActivity.class);
                    context.startActivity(intent2);
                    intent2.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                }

                break;

        }
    }
}
