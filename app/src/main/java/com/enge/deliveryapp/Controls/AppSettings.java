package com.enge.deliveryapp.Controls;

import android.content.Context;

public class AppSettings {


    public static void setAccessToken(Context context, String accessToken){

        SharedPreferencesControl.savePreferencesString(context,SharedPreferencesControl.ACCESS_TOKEN,accessToken);

    }

    public static String getAccessToken(Context context){

        return SharedPreferencesControl.loadSavedPreferencesString(context,SharedPreferencesControl.ACCESS_TOKEN);

    }

    public static String getAuthorization(Context context){

        return "Bearer "+SharedPreferencesControl.loadSavedPreferencesString(context,SharedPreferencesControl.ACCESS_TOKEN);

    }

    public static boolean isLoggedIn(Context context) {
        return SharedPreferencesControl.loadSavedPreferences(context, SharedPreferencesControl.LOGGED_IN) == 1;
    }

    public static void setLoggedIn(Context context, boolean loggedIn) {
        SharedPreferencesControl.savePreferences(context, SharedPreferencesControl.LOGGED_IN, 1);


    }

    public static String getRole(Context context) {

        return SharedPreferencesControl.loadSavedPreferencesRole(context, SharedPreferencesControl.ROLE);
    }

    public static void setRole(Context context, String role) {
        SharedPreferencesControl.savePreferencesRole(context, SharedPreferencesControl.ROLE, role);


    }

    public static String getUserName(Context context) {

        return SharedPreferencesControl.loadSavedPreferencesUsername(context, SharedPreferencesControl.USER_NAME);
    }

    public static void setUserName(Context context, String username) {
        SharedPreferencesControl.savePreferencesUsername(context, SharedPreferencesControl.USER_NAME, username);


    }

    public static String getPassword(Context context) {

        return SharedPreferencesControl.loadSavedPreferencesPassword(context, SharedPreferencesControl.PASSWORD);
    }

    public static void setPassword(Context context, String password) {
        SharedPreferencesControl.savePreferencesPassword(context, SharedPreferencesControl.PASSWORD, password);


    }

    public static void setId(Context context, String id) {
        SharedPreferencesControl.savePreferencesId(context, SharedPreferencesControl.ID, id);

    }
    public static String getId(Context context) {

        return SharedPreferencesControl.loadSavedPreferencesId(context, SharedPreferencesControl.ID);
    }
}
