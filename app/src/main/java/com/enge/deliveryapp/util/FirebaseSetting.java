package com.enge.deliveryapp.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.enge.deliveryapp.Activities.HomeActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import static android.support.constraint.Constraints.TAG;
import static com.enge.deliveryapp.Activities.HomeActivity.ONE_SECOND;

public class FirebaseSetting {
//    static Context context;
//    private static FirebaseAuth auth;
//    static FirebaseUser firebaseUser;
//    private static DatabaseReference reference;
//    String userId;
//    private static FusedLocationProviderClient mFusedLocationClient;
//    private static LocationRequest locationRequest;
//    private static LocationCallback locationCallback;
//    private double wayLatitude = 0.0, wayLongitude = 0.0;
//
//
//
//    FirebaseSetting(Context context){
//        this.context = context;
//        auth = FirebaseAuth.getInstance();
//        firebaseUser = auth.getCurrentUser();
//        userId = firebaseUser.getUid();
//
//        reference = FirebaseDatabase.getInstance().getReference("Users").child(userId);
//
//    }
//
//
//
//    public static void performLoginOrAccountCreation( final String username, final String password, final String role) {
//        Log.e("Main", "checking");
//
//        final String email = username + "@yahoo.com";
//
//        auth.fetchProvidersForEmail(email).addOnCompleteListener(
//                (Activity) context, new OnCompleteListener<ProviderQueryResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<ProviderQueryResult> task) {
//                        if (task.isSuccessful()) {
//                            Log.e("Main", "checking to see if user exists in firebase or not");
//
//                            ProviderQueryResult result = task.getResult();
//
//                            if (result != null && result.getProviders() != null
//                                    && result.getProviders().size() > 0) {
//                                Log.e("Main", "User exists, trying to login using entered credentials");
//                                performLogin(email, password, role);
//                            } else {
//                                Log.e("Main", "User doesn't exist, creating account");
//                                registerAccount(email,username, password, role);
//                            }
//                        } else {
//                            Log.e("Main", "User check failed", task.getException());
//
//                        }
//                    }
//                });
//    }
//
//    public static void registerAccount(final String username, final String email, String password, final String role) {
//
//        //create user
//        auth.createUserWithEmailAndPassword(email, password)
//                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if (task.isSuccessful()) {
//                            firebaseUser = auth.getCurrentUser();
//
//                            if (!role.equals("Distributor")) {
//                                initLocation();
//                                getLocation();
//                            }
//
//                            assert firebaseUser != null;
//                            String userId = firebaseUser.getUid();
//
//                            reference = FirebaseDatabase.getInstance().getReference("Users").child(userId);
//
//                            HashMap<String, String> hashMap = new HashMap<>();
//                            hashMap.put("id", userId);
//                            hashMap.put("role", role);
//                            hashMap.put("username", username);
//
//                            reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
//                                @Override
//                                public void onComplete(@NonNull Task<Void> task) {
//                                    if (task.isSuccessful()) {
//                                        Log.d(TAG, "account created");
//                                    } else {
//                                        Toast.makeText(context,
//                                                "account registration failed.",
//                                                Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//                            });
//
//                        } else {
//
//                            Log.w("Main", "Authentication failed." + task.getException());
//
//                            Toast.makeText(context, "Authentication failed." + task.getException(),
//                                    Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//
//    }
//
//    public static void performLogin(String email, String password, final String role) {
//
//        Log.e("Main", "login ");
//        //authenticate user
//        auth.signInWithEmailAndPassword(email, password)
//                .addOnCompleteListener((Activity) context, new OnCompleteListener<AuthResult>() {
//                    @Override
//                    public void onComplete(@NonNull Task<AuthResult> task) {
//                        if (task.isSuccessful()) {
//                            Log.e("Main", "login success");
//
//                            firebaseUser = auth.getCurrentUser();
//
//                            if (!role.equals("Distributor")) {
//                              //  initLocation();
//                               // getLocation();
//                            }
//
//                        } else {
//                            Toast.makeText((Activity)context,
//                                    "Authentication failed.",
//                                    Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                });
//
//    }
//
////    private static void initLocation() {
////
////        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
////
////        locationRequest = LocationRequest.create();
////        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
////        locationRequest.setInterval(10 * ONE_SECOND); // 10 seconds
////        locationRequest.setFastestInterval(5 * ONE_SECOND); // 5 seconds
////
////        locationCallback = new LocationCallback() {
////            @Override
////            public void onLocationResult(LocationResult locationResult) {
////                if (locationResult == null || firebaseUser == null) {
////                    return;
////                }
////                for (Location location : locationResult.getLocations()) {
////                    if (location != null) {
////                        wayLatitude = location.getLatitude();
////                        wayLongitude = location.getLongitude();
////                        sendLocation(id, role, username, wayLatitude, wayLongitude);
////
////                    }
////                }
////            }
////        };
////    }
////
////    private static void getLocation() {
////        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
////                && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
////
////            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
////                    LOCATION_REQUEST);
////
////        } else {
////
////            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
////        }
////    }
////
////    private void sendLocation(String userId, String role, String username, double wayLatitude, double wayLongitude) {
////
////        if (auth.getCurrentUser() != null) {
////            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Locations")
////                    .child(auth.getCurrentUser().getUid());
////
////            HashMap<String, Object> hashMap = new HashMap<>();
////            hashMap.put("id", userId);
////            hashMap.put("role", role);
////            hashMap.put("name", username);
////            hashMap.put("lat", wayLatitude);
////            hashMap.put("long", wayLongitude);
////            //TODO update in single value
////            reference.updateChildren(hashMap);
////            //TODO remove toast
////            Toast.makeText(mContext, "update location", Toast.LENGTH_SHORT).show();
////        }
////
////    }
////
////    @SuppressLint("MissingPermission")
////    @Override
////    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
////        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
////
////        switch (requestCode) {
////            case LOCATION_REQUEST: {
////
////                // If request is cancelled, the result arrays are empty.
////                if (grantResults.length > 0
////                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
////                    mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
////
////                } else {
////                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
////                }
////                break;
////            }
////
////        }
////    }

}
