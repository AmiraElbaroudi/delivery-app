package com.enge.deliveryapp.Remote;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.internal.JavaNetCookieJar;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {


    public static String BASE_URL = "http://service.snapmat.se/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {

        if (retrofit == null) {

            //logging
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            // log level
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            // init cookie manager
            CookieHandler cookieHandler = new CookieManager();

            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addNetworkInterceptor(logging)
                    .cookieJar(new JavaNetCookieJar(cookieHandler))
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }
}
