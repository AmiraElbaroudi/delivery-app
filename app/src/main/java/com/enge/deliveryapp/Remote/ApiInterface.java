package com.enge.deliveryapp.Remote;

import com.enge.deliveryapp.model.DeliveryBoys;
import com.enge.deliveryapp.model.Order;
import com.enge.deliveryapp.model.OrderData;
import com.enge.deliveryapp.model.OrderDetails;
import com.enge.deliveryapp.model.OrderDistributor;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;

import java.util.ArrayList;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @Headers({"Content-Type:application/x-www-form-urlencoded", "Accept:application/json"})
    @POST("oauth/token")
    @FormUrlEncoded
    Observable<Response<Object>> login(@Field("username") String email,
                                       @Field("password") String password,
                                       @Field("grant_type") String grant_type);

    @Headers({"Content-Type:application/json"})
    @POST("api/accounts/SaveRegistrationId")
    Observable<Response<Object>> saveRegistrationId(@Body JsonObject paramObject, @Header("Authorization") String auth);

    @Headers({"Content-Type:application/json"})
    @POST("api/accounts/Logout")
    Observable<Response<Object>> logout(@Body JsonObject paramObject, @Header("Authorization") String auth);

    @GET("api/Distributor/AllOrdersByCity")
    Observable<Response<OrderDistributor>> getAllOrdersDistributor(@Header("Authorization") String auth);

    @Headers({"Content-Type:application/json"})
    @POST("api/Distributor/ChangeOrdersTime")
    Observable<Response<Object>> changeOrdersTime(@Body JsonObject paramObject, @Header("Authorization") String auth);

    @GET("api/Distributor/CarMotorStatusByCity")
    Observable<Response<DeliveryBoys>> getCarMotorStatus(@Header("Authorization") String auth);

    @GET("api/Distributor/getOrderData")
    Observable<Response<OrderData>> getOrderDataDistributor(@Query("orderId") int orderId, @Header("Authorization") String auth);

    @GET("api/Distributor/SendOrderForAllCars")
    Observable<Response<Object>> sendOrderForAllCars(@Query("orderId") int orderId, @Header("Authorization") String auth);

    @GET("api/Distributor/SendOrderForAllMotors")
    Observable<Response<Object>> sendOrderForAllMotors(@Query("orderId") int orderId, @Header("Authorization") String auth);

    @GET("api/Distributor/AssignOrderToDeliveryBoy")
    Observable<Response<Object>> assignOrderToDeliveryBoy(@Query("orderId") int orderId, @Query("DeliveryBoyId") int deliveryBoyId,  @Header("Authorization") String auth);


//---------------------             Delivery Boy           --------------------------------------------------------

    @GET("api/DeliveryBoy/AllOrders")
    Observable<Response<LinkedTreeMap<String,ArrayList<Order>>>>  getAllOrdersCarMotor(@Header("Authorization") String auth);

    @GET("api/DeliveryBoy/getOrderData")
    Observable<Response<OrderData>> getOrderDataDeliveryBoy(@Query("orderId") int orderId, @Header("Authorization") String auth);

    @GET("api/DeliveryBoy/AcceptOrder/{orderId}")
    Observable<Response<Object>> acceptOrder (@Path("orderId") int orderId, @Header("Authorization") String auth);

    @Headers({"Content-Type:application/json"})
    @POST("api/DeliveryBoy/CheckInOrder")
    Observable<Response<Object>> checkInOrders(@Body JsonObject paramObject,@Header("Authorization") String auth);

    @GET("api/DeliveryBoy/TakeOrder/{orderId}")
    Observable<Response<Object>> takeOrder (@Path("orderId") int orderId, @Header("Authorization") String auth);

    @Headers({"Content-Type:application/json"})
    @POST("api/DeliveryBoy/DeliverToCustomer")
    Observable<Response<Object>> deliverToCustomer(@Body JsonObject paramObject, @Header("Authorization") String auth);

//--------------------------------------------------------------------------------------------------------------

    @GET("api/order/GetorderDetails/{orderId}")
    Observable<Response<OrderDetails>> getOrderDetails(@Path("orderId") int orderId, @Header("Authorization") String auth);

}
