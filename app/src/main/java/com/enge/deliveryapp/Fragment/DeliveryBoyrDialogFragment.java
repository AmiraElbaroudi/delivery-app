package com.enge.deliveryapp.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.enge.deliveryapp.Adapters.DeliveryBoysListDialogAdapter;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.model.DeliveryBoy;

import java.util.ArrayList;

public class DeliveryBoyrDialogFragment extends DialogFragment {

    public static final String DATA = "items";
    DeliveryBoysListDialogAdapter adapter;
    private OnReceivedDeliveryBoy deliveryBoy_checked;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_delivery_boy_dialog, null);

        Bundle bundle = getArguments();
        ArrayList<DeliveryBoy> deliveryBoy = (ArrayList<DeliveryBoy>) bundle.get(DATA);

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setView(view);
        //dialog.setView(R.layout.fragment_delivery_boy_dialog);
        dialog.setTitle(R.string.please_select_delivery_boy);
        dialog.setPositiveButton(R.string.ok, new PositiveButtonClickListener());
        dialog.setNegativeButton(R.string.cancel, new NegativeButtonClickListener());

        //RECYCER
        RecyclerView  rv=  view.findViewById(R.id.list_driver);
        rv.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        rv.setHasFixedSize(true);


        //ADAPTER
         adapter = new DeliveryBoysListDialogAdapter(this.getActivity(),deliveryBoy);
         rv.setAdapter(adapter);

        return dialog.create();
    }

    class PositiveButtonClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            //Get the delivery boy checked and send to activity
            deliveryBoy_checked.sendDeliveryBoy( adapter.getSelectedItem());
            dialog.dismiss();
        }
    }

    class NegativeButtonClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    public interface OnReceivedDeliveryBoy{
        public void sendDeliveryBoy(DeliveryBoy deliveryBoy);
    }

    // This makes sure that the container activity has implemented
    // the callback interface. If not, it throws an exception
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnReceivedDeliveryBoy) {
            deliveryBoy_checked = (OnReceivedDeliveryBoy) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnReceivedDeliveryBoy");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        deliveryBoy_checked = null;
    }
//    List<String> list = new ArrayList<>();
//    CharSequence[] cs;
//
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//
//
//
//        View rootView = inflater.inflate(R.layout.fragment_delivery_boy_dialog, container, false);
//        getDialog().setTitle(R.string.please_select_delivery_boy);
//        return rootView;
//    }

}