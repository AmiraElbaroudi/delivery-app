package com.enge.deliveryapp.Activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.enge.deliveryapp.Adapters.UsersListAdapter;
import com.enge.deliveryapp.Controls.AppSettings;
import com.enge.deliveryapp.Controls.BottomNavigationSetting;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ActivityChatBinding;
import com.enge.deliveryapp.model.ChatList;
import com.enge.deliveryapp.model.User;
import com.enge.deliveryapp.viewModels.LoginViewModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener {

    Context mContext = ChatActivity.this;
    ActivityChatBinding binding;

    private ArrayList<User> users  ;
    private ArrayList<String> chatLists;
    private UsersListAdapter usersListAdapter;

    FirebaseUser firebaseUser;
    DatabaseReference reference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat);

        binding.layoutBottom.btnChat.setOnClickListener(this);
        binding.layoutBottom.btnOrder.setOnClickListener(this);

        String username = AppSettings.getUserName(mContext);
        binding.username.setText(username);

        binding.listChat.setLayoutManager(new LinearLayoutManager(this));
        binding.listChat.setItemAnimator(new DefaultItemAnimator());
        binding.listChat.addItemDecoration(new DividerItemDecoration(binding.listChat.getContext(), DividerItemDecoration.VERTICAL));
        binding.listChat.setHasFixedSize(true);

        binding.swiperefreshlayout.setOnRefreshListener(this);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        chatLists = new ArrayList<>();
        getChatUser();

    }

    private void getChatUser() {

        if (firebaseUser != null){
            reference = FirebaseDatabase.getInstance().getReference("user-messages").child(firebaseUser.getUid());
            reference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    chatLists.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()){

                        String chatlist = snapshot.getKey();
                        chatLists.add(chatlist);
                        binding.chatListProgressbar.setVisibility(View.VISIBLE);
                    }
                    chatList();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    binding.chatListProgressbar.setVisibility(View.INVISIBLE);
                }
            });

        }
    }

    private void chatList() {

        users = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference("Users");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                users.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    User user = snapshot.getValue(User.class);
                    for (String chatlist : chatLists){
                        assert user != null;
                        if (user.getId().equals(chatlist)){
                            users.add(user);
                        }
                    }
                }
                binding.chatListProgressbar.setVisibility(View.INVISIBLE);

                usersListAdapter = new UsersListAdapter(mContext, users, true);
                binding.listChat.setAdapter(usersListAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                binding.chatListProgressbar.setVisibility(View.INVISIBLE);
            }
        });

    }


    @Override
    public void onClick(View view) {
        BottomNavigationSetting.enableNavigation(mContext, this, view);

    }


    public void showUserActivity(View view) {
        Intent intent = new Intent(ChatActivity.this, UserActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
       // binding.chatListProgressbar.setVisibility(View.VISIBLE);
        getChatUser();
        binding.swiperefreshlayout.setRefreshing(false);

    }
}
