package com.enge.deliveryapp.Activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.enge.deliveryapp.Adapters.DeliveryBoysListAdapter;
import com.enge.deliveryapp.Adapters.OrdersListDeliveryboyAdapter;
import com.enge.deliveryapp.Adapters.OrdersListDeliveryboyAdapter.OrdersDeliveryboyAdapterListener;
import com.enge.deliveryapp.Adapters.OrdersListDistributorAdapter;
import com.enge.deliveryapp.Controls.AppSettings;
import com.enge.deliveryapp.Controls.BottomNavigationSetting;
import com.enge.deliveryapp.model.Car;
import com.enge.deliveryapp.model.DeliveryBoy;
import com.enge.deliveryapp.model.DeliveryBoys;
import com.enge.deliveryapp.model.Motor;
import com.enge.deliveryapp.model.Order;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ActivityHomeBinding;
import com.enge.deliveryapp.viewModels.LoginViewModel;
import com.enge.deliveryapp.viewModels.OrdersViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.ProviderQueryResult;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import static android.support.constraint.Constraints.TAG;
import static com.enge.deliveryapp.Adapters.DeliveryBoysListAdapter.TYPE_CAR;
import static com.enge.deliveryapp.Adapters.DeliveryBoysListAdapter.TYPE_MOTOR;
import static com.enge.deliveryapp.Adapters.OrdersListDistributorAdapter.*;

public class HomeActivity extends AppCompatActivity implements Observer,
        SwipeRefreshLayout.OnRefreshListener,
        OrdersDeliveryboyAdapterListener,
        OrdersDistributorAdapterListener,
        AdapterView.OnItemSelectedListener,
        View.OnClickListener {

    public static final int LOCATION_REQUEST = 1000;
    public static final int ONE_SECOND = 1000;
    private static FusedLocationProviderClient mFusedLocationClient;
    ArrayList<DeliveryBoy> cars = new ArrayList<>();
    ArrayList<DeliveryBoy> motors = new ArrayList<>();
    boolean update = false;
    OrdersListDeliveryboyAdapter deliveryboyAdapter;
    OrdersListDistributorAdapter adapter;
    FirebaseUser firebaseUser;
    private FirebaseAuth.AuthStateListener authListener;
    private ActivityHomeBinding binding;
    final android.arch.lifecycle.Observer<Boolean> loadingObserver = new android.arch.lifecycle.Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean isLoading) {
            if (isLoading != null) {
                if (isLoading) {
                    binding.orderListProgressbar.setVisibility(View.VISIBLE);

                } else {
                    binding.orderListProgressbar.setVisibility(View.INVISIBLE);
                }
            }

        }
    };
    final android.arch.lifecycle.Observer<Boolean> loadingBoyObserver = new android.arch.lifecycle.Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean isLoading) {
            if (isLoading != null) {
                if (isLoading) {
                    binding.DeliveryBoyListProgressbar.setVisibility(View.VISIBLE);

                } else {
                    binding.DeliveryBoyListProgressbar.setVisibility(View.INVISIBLE);
                }
            }

        }
    };
    private OrdersViewModel ordersViewModel;
    private LoginViewModel loginViewModel;
    private Context mContext = HomeActivity.this;
    private int selectedItem = 0;
    private ArrayList<Order> deliverdOrders = new ArrayList<>();
    private ArrayList<Order> preparedOrders = new ArrayList<>();
    private ArrayList<Order> lateOrders = new ArrayList<>();
    private String time;
    private ArrayList<Order> deliverdOrders_Deliveryboy;
    private ArrayList<Order> notDeliverdOrders;
    private ArrayList<Order> lateOrders_Deliveryboy;
    private ArrayList<Order> notAcceptedOrder;
    private DeliveryBoys deliveryBoys;
    private String role;
    private String username;
    private String password;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    // Create the observer which updates the UI.
    final android.arch.lifecycle.Observer<String> toastObserver = new android.arch.lifecycle.Observer<String>() {
        @Override
        public void onChanged(@Nullable final String msg) {
            assert msg != null;
            if (msg.equals(getResources().getString(R.string.logout_successfully))) {
                //stop location updates when Activity is no longer active
                if (mFusedLocationClient != null) {
                    mFusedLocationClient.removeLocationUpdates(locationCallback);
                }
                FirebaseAuth.getInstance().signOut();
            }

            Toast.makeText(HomeActivity.this, msg, Toast.LENGTH_LONG).show();
        }
    };
    private double wayLatitude, wayLongitude;
    private FirebaseAuth auth;
    private DatabaseReference reference;
    private String id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);

        ordersViewModel = new OrdersViewModel(HomeActivity.this);
        loginViewModel = new LoginViewModel(HomeActivity.this);


        setUpObserver(ordersViewModel);
        ordersViewModel.getToastMessage().observe(this, toastObserver);
        ordersViewModel.getIsLoadingOrders().observe(this, loadingObserver);
        ordersViewModel.getIsLoadingDeliveryBoy().observe(this, loadingBoyObserver);


        binding.setViewModel(ordersViewModel);
        binding.swiperefreshlayout.setOnRefreshListener(this);
        binding.layoutBottom.btnChat.setOnClickListener(this);
        binding.layoutBottom.btnOrder.setOnClickListener(this);
        binding.spinner.setOnItemSelectedListener(this);


        role = AppSettings.getRole(mContext);
        username = AppSettings.getUserName(mContext);
        password = AppSettings.getPassword(mContext);
        id = AppSettings.getId(mContext);

        auth = FirebaseAuth.getInstance();
        //get current user
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    // user auth state is changed - user is null
                    // login
                    performLoginOrAccountCreation();

                }
            }
        };

        performLoginOrAccountCreation();

        // check type of user
        if (!role.equals("Distributor")) {
            binding.icCheckedGreen.setChecked(true);
            binding.icCheckedGreen.setSelected(true);

            binding.yellowLayout.setVisibility(View.INVISIBLE);

            binding.timeLayout.setVisibility(View.INVISIBLE);
            binding.driversLists.setVisibility(View.GONE);

            ordersViewModel.getAllOrdersCarMotor();
        }
        else {
            binding.icCheckedYellow.setChecked(true);
            binding.icCheckedYellow.setSelected(true);

            binding.icCheckedGreen.setChecked(true);
            binding.icCheckedGreen.setSelected(true);

            ordersViewModel.getAllOrdersDistributor();
            ordersViewModel.getAllCarMotorStatus();

            initSpinner();
        }

        initRecyclerView();

    }

    private void performLoginOrAccountCreation() {

        final String email = username + "@yahoo.com";

        auth.fetchProvidersForEmail(email).addOnCompleteListener(
                HomeActivity.this, new OnCompleteListener<ProviderQueryResult>() {
                    @Override
                    public void onComplete(@NonNull Task<ProviderQueryResult> task) {
                        if (task.isSuccessful()) {

                            ProviderQueryResult result = task.getResult();

                            if (result != null && result.getProviders() != null
                                    && result.getProviders().size() > 0) {
                                performLogin(email, password);
                            } else {
                                registerAccount(email, password, role);
                            }
                        } else {
                            Log.e("Main", "User check failed", task.getException());
                        }
                    }
                });
    }

    public void registerAccount(final String email, String password, final String role) {

        //create user
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            firebaseUser = auth.getCurrentUser();

                            if (!role.equals("Distributor")) {
                                initLocation();
                                getLocation();
                            }

                            assert firebaseUser != null;
                            String userId = firebaseUser.getUid();

                            reference = FirebaseDatabase.getInstance().getReference("Users").child(userId);

                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", userId);
                            hashMap.put("role", role);
                            hashMap.put("username", username);

                            reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "account created");
                                    } else {
                                        Toast.makeText(HomeActivity.this,
                                                "account registration failed.",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                        } else {

                            Log.w("Main", "Authentication failed." + task.getException());

                            Toast.makeText(HomeActivity.this, "Authentication failed." + task.getException(),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    public void performLogin(String email, String password) {
        if (auth != null) {
            //authenticate user
            auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {

                                Log.d("Main", "login success");

                                firebaseUser = auth.getCurrentUser();

                                if (!role.equals("Distributor")) {
                                    initLocation();
                                    getLocation();
                                }
                            } else {
                                Toast.makeText(HomeActivity.this,
                                        "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }

    }

    private void initLocation() {

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * ONE_SECOND); // 10 seconds
        locationRequest.setFastestInterval(5 * ONE_SECOND); // 5 seconds

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null || firebaseUser == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        wayLatitude = location.getLatitude();
                        wayLongitude = location.getLongitude();
                        sendLocation(id, role, username, wayLatitude, wayLongitude);

                    }
                }
            }
        };
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    LOCATION_REQUEST);
        } else {
            mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
        }
    }

    private void sendLocation(String userId, String role, String username, double wayLatitude, double wayLongitude) {

        if (auth.getCurrentUser() != null) {
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Locations")
                    .child(auth.getCurrentUser().getUid());

            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("id", userId);
            hashMap.put("role", role);
            hashMap.put("name", username);
            hashMap.put("lat", wayLatitude);
            hashMap.put("long", wayLongitude);
            reference.updateChildren(hashMap);
        }

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case LOCATION_REQUEST: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);

                } else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    /**
     * Spinner initialization
     */
    public void initSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.time_order, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        binding.spinner.setAdapter(adapter);
    }

    /**
     * Spinner setup
     */
    public void setupSpinner() {


        binding.spinner.setSelection(getIndex(time));
    }

    private int getIndex(String myString) {

        int index = 0;
        if (myString.equals("00:15"))
            index = 1;
        if (myString.equals("00:20"))
            index = 2;
        if (myString.equals("00:25"))
            index = 3;
        if (myString.equals("00:30"))
            index = 4;
        if (myString.equals("00:35"))
            index = 5;
        if (myString.equals("00:40"))
            index = 6;
        if (myString.equals("00:45"))
            index = 7;
        if (myString.equals("00:50"))
            index = 8;


        return index;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        selectedItem = position;

        String orderTime = "";
        if (position == 1)
            orderTime = "00:15";
        if (position == 2)
            orderTime = "00:20";
        if (position == 3)
            orderTime = "00:25";
        if (position == 4)
            orderTime = "00:30";
        if (position == 5)
            orderTime = "00:35";
        if (position == 6)
            orderTime = "00:40";
        if (position == 7)
            orderTime = "00:45";
        if (position == 8)
            orderTime = "00:50";

        if (!orderTime.isEmpty())
            ordersViewModel.changeOrderTime(orderTime);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * RecyclerView initialization
     */
    public void initRecyclerView() {
        binding.ordersList.setLayoutManager(new LinearLayoutManager(this));
        binding.ordersList.setItemAnimator(new DefaultItemAnimator());
        binding.ordersList.addItemDecoration(new DividerItemDecoration(binding.ordersList.getContext(), DividerItemDecoration.VERTICAL));

        binding.carsList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        binding.scootersList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

    }

    /**
     * RecyclerView setup
     */
    private void setupOrders_Deliveryboy_RecyclerView() {
        deliveryboyAdapter = new OrdersListDeliveryboyAdapter(this, deliverdOrders_Deliveryboy, notDeliverdOrders, lateOrders_Deliveryboy, notAcceptedOrder, this);
        binding.ordersList.setAdapter(deliveryboyAdapter);
    }

    private void setupOrders_Distributor_RecyclerView() {
        adapter = new OrdersListDistributorAdapter(this, deliverdOrders, preparedOrders, lateOrders, this);
        binding.ordersList.setAdapter(adapter);
    }

    private void setupCarRecyclerView(ArrayList<DeliveryBoy> cars) {
        DeliveryBoysListAdapter carAdapter = new DeliveryBoysListAdapter(this, cars);
        binding.carsList.setAdapter(carAdapter);
    }

    private void setupMotorRecyclerView(ArrayList<DeliveryBoy> motors) {

        DeliveryBoysListAdapter motorAdapter = new DeliveryBoysListAdapter(this, motors);
        binding.scootersList.setAdapter(motorAdapter);

    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof OrdersViewModel) {

            if (role.equals("Distributor")) {
                deliverdOrders = ordersViewModel.getDeliverdOrdersArr();
                preparedOrders = ordersViewModel.getPreparedOrdersArr();
                lateOrders = ordersViewModel.getLateOrdersArr();

                binding.numGreen.setText(String.valueOf(deliverdOrders.size()));
                binding.numYellow.setText(String.valueOf(preparedOrders.size()));

                setupOrders_Distributor_RecyclerView();

                time = ordersViewModel.getTime();
                if (time != null) {
                    setupSpinner();
                }

                deliveryBoys = ordersViewModel.getDeliveryBoys();
                if (deliveryBoys != null) {
                    cars = genericDeliveryBoy(deliveryBoys, TYPE_CAR);
                    motors = genericDeliveryBoy(deliveryBoys, TYPE_MOTOR);

                    setupCarRecyclerView(cars);
                    setupMotorRecyclerView(motors);
                }

            } else {
                deliverdOrders_Deliveryboy = ordersViewModel.getDeliverdOrdersDeliveryboy();
                notDeliverdOrders = ordersViewModel.getNotDeliverdOrdersArr();
                lateOrders_Deliveryboy = ordersViewModel.getLateOrdersDeliveryboy();
                notAcceptedOrder = ordersViewModel.getNotAcceptedOrderArr();

                if (deliverdOrders_Deliveryboy != null)
                    binding.numGreen.setText(String.valueOf(deliverdOrders_Deliveryboy.size()));

                setupOrders_Deliveryboy_RecyclerView();
            }


        }
    }

    public void setUpObserver(Observable observable) {
        observable.addObserver(this);
    }

    // get generic of array list of deliveryBoy
    public ArrayList<DeliveryBoy> genericDeliveryBoy(DeliveryBoys deliveryBoys, int type) {

        ArrayList<DeliveryBoy> boys = new ArrayList<>();

        if (type == TYPE_CAR) {
            ArrayList<Car> myCars = deliveryBoys.getCars();
            Car car = new Car();
            for (int i = 0; i < myCars.size(); i++) {
                car = myCars.get(i);

                boys.add(new DeliveryBoy(car.getCarId(), car.getName(), car.getCarNo(), car.getnOfOrder()));
            }
            return boys;
        } else {
            ArrayList<Motor> myMotors = deliveryBoys.getMotors();
            Motor motor = new Motor();
            for (int i = 0; i < myMotors.size(); i++) {
                motor = myMotors.get(i);

                boys.add(new DeliveryBoy(motor.getMotorId(), motor.getName(), motor.getMotorNo(), motor.getnOfOrder()));
            }
            return boys;
        }
    }

    public void setNumberOfOrderDelivered(View view) {

        if (!binding.icCheckedGreen.isSelected()) {
            if (role.equals("Distributor")) {
                binding.numGreen.setText(String.valueOf(deliverdOrders.size()));
            } else {
                binding.numGreen.setText(String.valueOf(deliverdOrders_Deliveryboy.size()));
            }
            binding.icCheckedGreen.setChecked(true);
            binding.icCheckedGreen.setSelected(true);
        } else {
            binding.numGreen.setText(" ");

            binding.icCheckedGreen.setChecked(false);
            binding.icCheckedGreen.setSelected(false);
        }

    }

    public void setNumberOfOrderNotDeliver(View view) {
        if (!binding.icCheckedYellow.isSelected()) {
            binding.numYellow.setText(String.valueOf(preparedOrders.size()));

            binding.icCheckedYellow.setChecked(true);
            binding.icCheckedYellow.setSelected(true);
        } else {
            binding.numYellow.setText(" ");

            binding.icCheckedYellow.setChecked(false);
            binding.icCheckedYellow.setSelected(false);
        }
    }


    //start Order details activity
    @Override
    public void onOrderClickedDistributor(Order order, int position, String orderType) {

        Intent intent = new Intent(HomeActivity.this, AddressActivity.class);

        Bundle extra = new Bundle();

        extra.putString(AddressActivity.ORDER_TYPE, orderType);
        extra.putSerializable(AddressActivity.ORDER, order);
        extra.putSerializable(AddressActivity.CARS, cars);
        extra.putSerializable(AddressActivity.MOTORS, motors);

        intent.putExtra(AddressActivity.EXTRA, extra);

        startActivity(intent);
    }

    @Override
    public void onOrderClickedDeliveryboy(Order order, int position, String orderType) {

        Intent intent = new Intent(HomeActivity.this, AddressActivity.class);
        Bundle extra = new Bundle();

        extra.putString(AddressActivity.ORDER_TYPE, orderType);
        extra.putSerializable(AddressActivity.ORDER, order);
        extra.putSerializable(AddressActivity.CARS, cars);
        extra.putSerializable(AddressActivity.MOTORS, motors);

        intent.putExtra(AddressActivity.EXTRA, extra);

        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        if (role.equals("Distributor")) {
            binding.icCheckedGreen.setChecked(true);
            binding.icCheckedGreen.setSelected(true);

            binding.icCheckedYellow.setChecked(true);
            binding.icCheckedYellow.setSelected(true);

            ordersViewModel.getAllOrdersDistributor();
            ordersViewModel.getAllCarMotorStatus();
        } else {
            binding.icCheckedGreen.setChecked(true);
            binding.icCheckedGreen.setSelected(true);

            ordersViewModel.getAllOrdersCarMotor();
        }
        binding.swiperefreshlayout.setRefreshing(false);
    }

    @Override
    public void onClick(View view) {
        BottomNavigationSetting.enableNavigation(mContext, this, view);

    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }
    }

    public void logout(View view) {
        loginViewModel.onLogoutClicked();
    }

}

