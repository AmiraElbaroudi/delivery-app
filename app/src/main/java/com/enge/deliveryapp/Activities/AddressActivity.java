package com.enge.deliveryapp.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.databinding.DataBindingUtil;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.enge.deliveryapp.Controls.AppSettings;
import com.enge.deliveryapp.Enums.DeliveryStatusEnum;
import com.enge.deliveryapp.Fragment.DeliveryBoyrDialogFragment;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ActivityAdressBinding;
import com.enge.deliveryapp.model.DeliveryBoy;
import com.enge.deliveryapp.model.Order;
import com.enge.deliveryapp.model.RestaurantData;
import com.enge.deliveryapp.model.UserCoord;
import com.enge.deliveryapp.model.UserData;
import com.enge.deliveryapp.viewModels.AddressViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class AddressActivity extends AppCompatActivity implements DeliveryBoyrDialogFragment.OnReceivedDeliveryBoy,
        Observer, SwipeRefreshLayout.OnRefreshListener {

    public static final String EXTRA = "extra";
    public static final String ORDER = "order";
    public static final String CARS = "cars";
    public static final String MOTORS = "motors";
    public static final String IS_PREPARED = "isPrepared";
    public static final String ORDER_TYPE = "order_type";

    public static final int TYPE_CAR = 0;
    public static final int TYPE_MOTOR = 1;
    public static final int REQUEST_CODE = 1;
    private ActivityAdressBinding binding;
    // Create the observer which updates the UI.
    final android.arch.lifecycle.Observer<String> toastObserver = new android.arch.lifecycle.Observer<String>() {
        @Override
        public void onChanged(@Nullable final String msg) {
            if (msg != null) {
                Toast.makeText(AddressActivity.this, msg, Toast.LENGTH_LONG).show();

                if (msg.equals(getResources().getString(R.string.accept_order))) {
                    binding.txtButton.setText(getString(R.string.check_in_order));
                } else if (msg.equals(getResources().getString(R.string.check_in))) {
                    binding.txtButton.setText(getString(R.string.show_order_items));
                    binding.btnAccept.setBackgroundColor(Color.GRAY);
                } else if (msg.equals(getResources().getString(R.string.deliver_to_customer))) {

                    Intent intent = new Intent(AddressActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }
    };
    final android.arch.lifecycle.Observer<Boolean> LoadingOrderDataDistributorObserver = new android.arch.lifecycle.Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean isLoading) {
            if (isLoading != null) {
                if (isLoading) {
                    binding.progressbar.setVisibility(View.VISIBLE);
                } else {
                    binding.progressbar.setVisibility(View.GONE);
                }
            }
        }
    };

    final android.arch.lifecycle.Observer<Boolean> LoadingOrderDataDeliveryboyObserver = new android.arch.lifecycle.Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean isLoading) {
            if (isLoading != null) {
                if (isLoading) {
                    binding.progressbar.setVisibility(View.VISIBLE);
                } else {
                    binding.progressbar.setVisibility(View.GONE);
                }
            }
        }
    };
    final android.arch.lifecycle.Observer<Boolean> isAcceptObserver = new android.arch.lifecycle.Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean isLoading) {
            if (isLoading != null) {
                if (isLoading) {
                    binding.progressbar.setVisibility(View.VISIBLE);
                    binding.btnAccept.setEnabled(false);
                } else {
                    binding.progressbar.setVisibility(View.GONE);
                    binding.btnAccept.setEnabled(true);
                }
            }
        }
    };
    final android.arch.lifecycle.Observer<Boolean> isCheckInObserver = new android.arch.lifecycle.Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean isLoading) {
            if (isLoading != null) {
                if (isLoading) {
                    binding.progressbar.setVisibility(View.VISIBLE);
                    binding.btnAccept.setEnabled(false);
                } else {
                    binding.progressbar.setVisibility(View.GONE);
                    binding.btnAccept.setEnabled(true);
                }
            }
        }
    };

    final android.arch.lifecycle.Observer<Boolean> isDeliverToCustomerObserver = new android.arch.lifecycle.Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean isLoading) {
            if (isLoading != null) {
                if (isLoading) {
                    binding.progressbar.setVisibility(View.VISIBLE);
                    binding.btnAccept.setEnabled(false);
                } else {
                    binding.progressbar.setVisibility(View.GONE);
                    binding.btnAccept.setEnabled(true);
                }
            }
        }
    };
    private AddressViewModel addressViewModel;
    private Context context = AddressActivity.this;

    private String role;
    private Order order;
    private UserData userData;
    private RestaurantData restaurantData;
    private UserCoord userCoord;

    private ArrayList<DeliveryBoy> cars = new ArrayList<>();
    private ArrayList<DeliveryBoy> motors = new ArrayList<>();

    private int deliveryBoyType;
    private int deliveryStatus;

    private int orderId;
    private String restaurantName;
    private String order_type;
    private double wayLatitude, wayLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_adress);

        addressViewModel = new AddressViewModel(AddressActivity.this);
        setUpObserver(addressViewModel);

        addressViewModel.getToastMessage().observe(this, toastObserver);
        addressViewModel.getIsLoadingOrderDataDistributor().observe(this, LoadingOrderDataDistributorObserver);
        addressViewModel.getIsLoadingOrderData().observe(this, LoadingOrderDataDeliveryboyObserver);

        addressViewModel.getIsAccept().observe(this, isAcceptObserver);
        addressViewModel.getIsCheckIn().observe(this, isCheckInObserver);
        addressViewModel.getIsDeliverToCustomer().observe(this, isDeliverToCustomerObserver);

        binding.setViewModel(addressViewModel);

        binding.swiperefreshlayout.setOnRefreshListener(this);

        Bundle extra = getIntent().getBundleExtra(EXTRA);
        if (extra != null) {
            order = (Order) extra.getSerializable(ORDER);
            cars = (ArrayList<DeliveryBoy>) extra.getSerializable(CARS);
            motors = (ArrayList<DeliveryBoy>) extra.getSerializable(MOTORS);

            orderId = order.getOrderId();
            restaurantName = order.getRestaurantName();
            deliveryStatus = order.getDeliveryStatus();
            order_type = extra.getString(ORDER_TYPE);

            binding.txtRestaurantName.setText(restaurantName);
            binding.orderTimesLay.tvPickUpTime.setText(getString(R.string.empty_time));
            binding.orderTimesLay.tvDeliverTime.setText(getString(R.string.empty_time));
        }

        // check type of user
        role = AppSettings.getRole(context);
        if (role.equals("Distributor")) {
            addressViewModel.getOrderDataDistributor(orderId);

            binding.bottom.setVisibility(View.INVISIBLE);
            binding.carDriver.setVisibility(View.VISIBLE);
            binding.motoDriver.setVisibility(View.VISIBLE);
            binding.carDriver.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deliveryBoyType = TYPE_CAR;
                    showDialog();
                }
            });
            binding.motoDriver.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    deliveryBoyType = TYPE_MOTOR;
                    showDialog();
                }
            });

        }
        else {
            addressViewModel.getOrderDataDataDeliveryBoy(orderId);

            if (order_type.equals("Delivered")) {
                binding.txtButton.setText(getString(R.string.show_order_items));
                binding.btnAccept.setBackgroundColor(Color.GRAY);
            }
            else if (order_type.equals("NotDelivered")) {
                stateDelivery();
            }
            else if (order_type.equals("notAccepted")) {
                binding.txtButton.setText(getString(R.string.accept));
            }
            else if (order_type.equals("lated")) {
                stateDelivery();
            }
        }
    }

    public void stateDelivery() {
        if (deliveryStatus == DeliveryStatusEnum.NOT_ACCEPTED.getCode()) {
            binding.txtButton.setText(getString(R.string.accept));
        }
        else if (deliveryStatus == DeliveryStatusEnum.ACCEPT_DELIVERY.getCode()) {
            binding.txtButton.setText(getString(R.string.check_in_order));
        }
        else if (order.getDeliveryStatus() == DeliveryStatusEnum.DELIVERY_ARRIVE_RESTAURANT.getCode()) {
            binding.txtButton.setText(getString(R.string.show_order_items));
            binding.btnAccept.setBackgroundColor(Color.GRAY);
        }
        else if (deliveryStatus == DeliveryStatusEnum.TAKEN.getCode()) {
            binding.txtButton.setText(getString(R.string.deliver_to_customer));
        }
        else {
            binding.txtButton.setText(getString(R.string.show_order_items));
            binding.btnAccept.setBackgroundColor(Color.GRAY);
        }
    }

    private void showDialog() {

        FragmentManager manager = getSupportFragmentManager();
        DeliveryBoyrDialogFragment dialog = new DeliveryBoyrDialogFragment();

        Bundle bundle = new Bundle();
        if (deliveryBoyType == TYPE_CAR) {
            bundle.putSerializable(DeliveryBoyrDialogFragment.DATA, getDeliveryBoyDialog(cars));

        } else if (deliveryBoyType == TYPE_MOTOR) {
            bundle.putSerializable(DeliveryBoyrDialogFragment.DATA, getDeliveryBoyDialog(motors));
        }
        dialog.setArguments(bundle);
        dialog.show(manager, "Dialog");
    }

    private ArrayList<DeliveryBoy> getDeliveryBoyDialog(ArrayList<DeliveryBoy> deliveryBoys) {

        ArrayList<DeliveryBoy> myDeliveryBoy = new ArrayList<>();

        for (int i = 0; i < deliveryBoys.size(); i++) {
            if (i == 0)
                myDeliveryBoy.add(new DeliveryBoy("For All"));
            myDeliveryBoy.add(deliveryBoys.get(i));
        }
        return myDeliveryBoy;
    }

    @Override
    public void sendDeliveryBoy(DeliveryBoy deliveryBoy) {
        if (deliveryBoy.getName().equals("For All")) {
            if (deliveryBoyType == TYPE_CAR) {
                addressViewModel.sendOrderForAllCars(orderId);

            } else if (deliveryBoyType == TYPE_MOTOR) {
                addressViewModel.sendOrderForAllMotors(orderId);
            }

        } else {
            int deliveryBoyId = deliveryBoy.getDeliveryBoyId();
            addressViewModel.assignOrderToDeliveryBoy(orderId, deliveryBoyId);
        }
    }

    public void backArrow(View view) {
        Intent intent = new Intent(AddressActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    public void getLocation(final String apiType) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        Task task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    wayLongitude = location.getLongitude();
                    wayLatitude = location.getLatitude();
                    if (apiType.equals("check in")) {
                        addressViewModel.checkInOrders(orderId, String.valueOf(wayLongitude), String.valueOf(wayLatitude));
                    }
                    else {
                        addressViewModel.deliverToCustomer(orderId, String.valueOf(wayLongitude), String.valueOf(wayLatitude));
                    }
                }
            }
        });
    }

    public void acceptOrder(View view) {
        String text = (String) binding.txtButton.getText();

        if (text.equals(getString(R.string.accept))) {
            addressViewModel.acceptOrder(orderId);

        } else if (text.equals(getString(R.string.check_in_order))) {
            getLocation("check in");

        } else if (text.equals(getString(R.string.show_order_items))) {
            Intent intent = new Intent(AddressActivity.this, OrderItemActivity.class);
            Bundle extra = new Bundle();
            extra.putString("restaurantName", restaurantName);
            extra.putSerializable("userData", userData);
            extra.putSerializable("restaurantData", restaurantData);
            extra.putSerializable("ORDER", order);
            extra.putString(ORDER_TYPE, order_type);
            intent.putExtra(EXTRA, extra);
            startActivityForResult(intent, REQUEST_CODE);

        } else if (text.equals(getString(R.string.deliver_to_customer))) {
            getLocation("delivery to customer");

        }
    }

    @Override
    public void onRefresh() {
        if (role.equals("Distributor")) {
            addressViewModel.getOrderDataDistributor(orderId);
        } else {
            addressViewModel.getOrderDataDataDeliveryBoy(orderId);
        }
        binding.swiperefreshlayout.setRefreshing(false);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof AddressViewModel) {
            restaurantData = addressViewModel.getRestaurantData();
            userData = addressViewModel.getUserData();
            userCoord = addressViewModel.getUserCoord();

            if (restaurantData != null && userData != null) {
                binding.setModel(restaurantData);
                binding.setModel2(userData);
                binding.orderTimesLay.tvPickUpTime.setText(restaurantData.getTimeFormatted(restaurantData.getOrderPickTime()));
                binding.orderTimesLay.tvDeliverTime.setText(restaurantData.getTimeFormatted(restaurantData.getOrderShouldArriveTime()));

            }
        }
    }

    private void setUpObserver(Observable observable) {
        observable.addObserver(this);
    }

    public void showOrderDetails(View view) {

        Intent intent = new Intent(AddressActivity.this, DetailsActivity.class);
        intent.putExtra("ORDER_ID", orderId);
        intent.putExtra("TYPE", order_type);
        startActivity(intent);
    }

    public void makeCall(View view) {
        TextView textView = (TextView) view;
        String number = textView.getText().toString().trim();
        Uri call = Uri.parse("tel:" + number);
        Intent surf = new Intent(Intent.ACTION_DIAL, call);
        startActivity(surf);
    }

    public void showMap(View view) {
        Intent intent = new Intent(AddressActivity.this, MapsActivity.class);

        Bundle extra = new Bundle();

        extra.putDouble("REST_LAT", Double.parseDouble(restaurantData.getRestLat()));
        extra.putDouble("REST_LNG", Double.parseDouble(restaurantData.getRestLng()));

        extra.putDouble("USER_LAT", userCoord.getLatitude());
        extra.putDouble("USER_LNG", userCoord.getLongitude());

        intent.putExtra(EXTRA, extra);

        startActivity(intent);
    }

}
