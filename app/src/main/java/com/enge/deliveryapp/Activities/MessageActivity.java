package com.enge.deliveryapp.Activities;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.enge.deliveryapp.Adapters.MessageAdapter;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ActivityMessageBinding;
import com.enge.deliveryapp.model.Message;
import com.enge.deliveryapp.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class MessageActivity extends AppCompatActivity {


    private static final int PICK_REQUEST = 1;
    Context mContext = MessageActivity.this;
    ActivityMessageBinding binding;
    Intent intent;
    FirebaseUser firebaseUser;
    DatabaseReference reference;
    StorageReference storageReference;
    MessageAdapter messageAdapter;
    ArrayList<Message> messages;
    private boolean initRecyclerView = true;
    private String toId;

    private String fromId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_message);

        binding.recyclerviewMessage.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplication());
        linearLayoutManager.setStackFromEnd(true);
        binding.recyclerviewMessage.setLayoutManager(linearLayoutManager);


        intent = getIntent();
        toId = intent.getStringExtra("userId");
        String username = intent.getStringExtra("username");

        binding.txtUsername.setText(username);

        storageReference = FirebaseStorage.getInstance().getReference();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        assert firebaseUser != null;
        fromId = firebaseUser.getUid();

        readMessage(fromId, toId);

        binding.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = binding.txtSend.getText().toString();
                if (!msg.equals("")) {
                    sendMessage(msg);
                } else {
                    Toast.makeText(MessageActivity.this, "You can't send empty message", Toast.LENGTH_SHORT).show();
                }
                binding.txtSend.setText("");
            }
        });

        binding.btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });
    }


    private void chooseImage() {
        Intent pickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        pickerIntent.setType("*/*");
        startActivityForResult(pickerIntent, PICK_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {

            Uri selectedMediaUri = data.getData();
            ContentResolver cr = mContext.getContentResolver();
            String mime = cr.getType(selectedMediaUri);

            //handle image
            assert mime != null;
            if (mime.toLowerCase().contains("image")) {
                upLoadImage(selectedMediaUri);
            }
            //handle video
            if (mime.toLowerCase().contains("video")) {
                upLoadVideo(selectedMediaUri);
            }
        }
    }

    public void upLoadImage(Uri imageUrl) {
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUrl);
            Bitmap bit = BitmapFactory.decodeFile(imageUrl.getPath());
        } catch (IOException e) {
            e.printStackTrace();
            FirebaseCrash.report(e);

        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        assert bitmap != null;
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        byte[] imageBytes = outputStream.toByteArray();

        String nameImage = UUID.randomUUID().toString();

        // Create file metadata including the content type
        StorageMetadata metadata = new StorageMetadata.Builder()
                .setContentType("application/octet-stream")
                .build();

        StorageReference ref = FirebaseStorage.getInstance().getReference()
                .child("message_images").child(nameImage);

        final Bitmap finalBitmap = bitmap;
        ref.putBytes(imageBytes, metadata).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    String downLoad_url = task.getResult().getDownloadUrl().toString();
                    sendMessageWithImageUrl(downLoad_url, finalBitmap);
                } else {
                    Toast.makeText(mContext, getString(R.string.failed_to_upload_image), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void upLoadVideo(Uri videoUrl) {
        String nameVideo = UUID.randomUUID().toString();

        StorageReference ref = FirebaseStorage.getInstance().getReference()
                .child("message_movies").child(nameVideo);

        ref.putFile(videoUrl).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    String downLoad_url = task.getResult().getDownloadUrl().toString();
                    sendMessageWithVideoUrl(downLoad_url);
                } else {
                    Toast.makeText(mContext, getString(R.string.failed_to_upload_video), Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public void sendMessageWithImageUrl(String downLoad_url, Bitmap bitmap) {
        DatabaseReference ref_message = FirebaseDatabase.getInstance().getReference().child("messages");
        final String push_id = ref_message.push().getKey();

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("fromId", fromId);
        hashMap.put("toId", toId);
        hashMap.put("timestamp", ServerValue.TIMESTAMP);
        hashMap.put("imageUrl", downLoad_url);
        hashMap.put("imageWidth", bitmap.getWidth());
        hashMap.put("imageHeight", bitmap.getHeight());

        ref_message.child(push_id).setValue(hashMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    sendUserMessage(push_id);
                }
            }
        });
    }

    public void sendMessageWithVideoUrl(String downLoad_url) {
        DatabaseReference ref_message = FirebaseDatabase.getInstance().getReference().child("messages");
        final String push_id = ref_message.push().getKey();

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("fromId", fromId);
        hashMap.put("toId", toId);
        hashMap.put("timestamp", ServerValue.TIMESTAMP);
        hashMap.put("videoUrl", downLoad_url);

        ref_message.child(push_id).setValue(hashMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    sendUserMessage(push_id);
                }
            }
        });
    }

    public void sendMessage(String message) {
        DatabaseReference ref_message = FirebaseDatabase.getInstance().getReference().child("messages");
        final String push_id = ref_message.push().getKey();

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("fromId", fromId);
        hashMap.put("toId", toId);
        hashMap.put("text", message);
        hashMap.put("timestamp", ServerValue.TIMESTAMP);

        ref_message.child(push_id).setValue(hashMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError == null) {
                    sendUserMessage(push_id);
                }
            }
        });
    }

    private void sendUserMessage(final String push_id) {

        final DatabaseReference ref_user_message = FirebaseDatabase.getInstance().getReference()
                .child("user-messages").child(fromId).child(toId);

        ref_user_message.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ref_user_message.child(push_id).setValue(1);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        final DatabaseReference ref_to_user_message = FirebaseDatabase.getInstance().getReference()
                .child("user-messages").child(toId).child(fromId);


        ref_to_user_message.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ref_to_user_message.child(push_id).setValue(1);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void readMessage(final String myId, final String userId) {
        messages = new ArrayList<>();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("messages");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                messages.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Message message = snapshot.getValue(Message.class);

                    assert message != null;
                    if (message.getToId().equals(myId) && message.getFromId().equals(userId) ||
                            message.getToId().equals(userId) && message.getFromId().equals(myId)) {
                        messages.add(message);
                    }
                }
                setupRecyclerView(messages);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void setupRecyclerView(ArrayList<Message> messages) {

        messageAdapter = new MessageAdapter(MessageActivity.this, messages);
        binding.recyclerviewMessage.setAdapter(messageAdapter);
    }

    public void backToChat(View view) {
        Intent intent = new Intent(MessageActivity.this, ChatActivity.class);
        startActivity(intent);
        finish();

    }
}
