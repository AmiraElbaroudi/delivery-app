package com.enge.deliveryapp.Activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.enge.deliveryapp.Adapters.OrderItemsDetailsListAdapter;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ActivityDetailsBinding;
import com.enge.deliveryapp.model.OrderDetails;
import com.enge.deliveryapp.model.OrderItem;
import com.enge.deliveryapp.viewModels.OrderDetailsViewModel;
import com.enge.deliveryapp.viewModels.OrdersViewModel;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import io.reactivex.annotations.Nullable;

public class DetailsActivity extends AppCompatActivity implements Observer, SwipeRefreshLayout.OnRefreshListener {

    ActivityDetailsBinding binding;

    final android.arch.lifecycle.Observer<Boolean> loadingObserver = new android.arch.lifecycle.Observer<Boolean>() {
        @Override
        public void onChanged(@android.support.annotation.Nullable Boolean isLoading) {
            if (isLoading != null) {
                if (isLoading) {
                    binding.itemOrderListProgressbar.setVisibility(View.VISIBLE);

                } else {
                    binding.itemOrderListProgressbar.setVisibility(View.INVISIBLE);
                }
            }
        }
    };

    OrderDetailsViewModel orderDetailsViewModel;
    Context mContext = DetailsActivity.this;
    OrderItemsDetailsListAdapter adapter;
    private RecyclerView recyclerView;
    private OrderDetails details = new OrderDetails();
    private ArrayList<OrderItem> orderItems = new ArrayList<OrderItem>();
    ;
    private int id;
    private Toast toast;

    final android.arch.lifecycle.Observer<String> toastOrderDetailsObserver = new android.arch.lifecycle.Observer<String>() {
        @Override
        public void onChanged(@Nullable final String msg) {
            binding.connectionErorLayout.setVisibility(View.VISIBLE);
            toast.setText(msg);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details);

        toast = Toast.makeText(this, "Toast", Toast.LENGTH_LONG);

        orderDetailsViewModel = new OrderDetailsViewModel(this);
        binding.setViewModel(orderDetailsViewModel);
        binding.setModel(new OrderDetails());

        setUpObserver(orderDetailsViewModel);
        orderDetailsViewModel.getToastMessageOrderDetails().observe(this, toastOrderDetailsObserver);
        orderDetailsViewModel.getIsLoading().observe(this, loadingObserver);

        binding.swiperefreshlayout.setOnRefreshListener(this);


        //RECEIVE OUR DATA
        Bundle extras = getIntent().getExtras();
        receiveData(extras);

        initRecyclerView();

    }

    /**
     * receive Data
     */
    public void receiveData(Bundle extras) {
        if (extras != null) {

            id = extras.getInt("ORDER_ID");
            String type = extras.getString("TYPE");

            orderDetailsViewModel.getOrderDetails(id);
        }
    }

    /**
     * RecyclerView initialization
     */
    public void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.orderItem_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
    }

    /**
     * RecyclerView setup
     */
    private void setupRecyclerView(ArrayList<OrderItem> data) {

        adapter = new OrderItemsDetailsListAdapter(this, data);
        recyclerView.setAdapter(adapter);
    }

    public void setUpObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof OrderDetailsViewModel) {

            if (orderDetailsViewModel.getDetails() != null) {
                binding.setModel(orderDetailsViewModel.getDetails());
                binding.detailsLayout2.setModel(orderDetailsViewModel.getDetails());
                binding.setModel(orderDetailsViewModel.getDetails());

                setupRecyclerView(orderDetailsViewModel.getDetails().getItems());

                details = orderDetailsViewModel.getDetails();
                orderItems = orderDetailsViewModel.getDetails().getItems();
            }
        }
    }

    @Override
    public void onRefresh() {
        orderDetailsViewModel.getOrderDetails(id);
        binding.connectionErorLayout.setVisibility(View.GONE);
        binding.swiperefreshlayout.setRefreshing(false);
    }

    public void backArrowOnClick(View view) {
        Intent intent = new Intent(DetailsActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    public void show_bill(View view) {
        Intent intent = new Intent(DetailsActivity.this, PrintActivity.class);

        Bundle extra = new Bundle();
        extra.putSerializable("details", details);
        extra.putSerializable("orderItems", orderItems);

        intent.putExtra("extra", extra);
        startActivity(intent);
    }
}