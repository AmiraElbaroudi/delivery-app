package com.enge.deliveryapp.Activities;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.enge.deliveryapp.R;
import com.enge.deliveryapp.viewModels.LoginViewModel;
import com.enge.deliveryapp.databinding.ActivityLoginBinding;
import java.util.Observable;
import java.util.Observer;

public class LoginActivity extends AppCompatActivity implements Observer {

    // Create the observer which updates the UI.
    final android.arch.lifecycle.Observer<String> toastObserver = new android.arch.lifecycle.Observer<String>() {
        @Override
        public void onChanged(@Nullable final String msg) {

            Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();

        }
    };

    final android.arch.lifecycle.Observer<Boolean> loadingObserver = new android.arch.lifecycle.Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean isLoading) {

            if (isLoading) {
                binding.loginProgressbar.setVisibility(View.VISIBLE);
                binding.btnLogin.setEnabled(false);

            } else {
                binding.loginProgressbar.setVisibility(View.INVISIBLE);
                binding.btnLogin.setEnabled(true);
            }
        }
    };
    LoginViewModel loginViewModel;
    ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        loginViewModel = new LoginViewModel(LoginActivity.this);
        setUpObserver(loginViewModel);
        loginViewModel.getToastMessage().observe(this, toastObserver);
        loginViewModel.getIsLoading().observe(this, loadingObserver);
        binding.setViewModel(loginViewModel);
    }

    public void setUpObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void update(Observable observable, Object o) {
        if (o instanceof LoginViewModel) {

        }
    }
}
