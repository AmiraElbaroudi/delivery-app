package com.enge.deliveryapp.Activities;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.enge.deliveryapp.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final int PADDING = 50;
    public static final float ZOOM = -3.0F;

    private GoogleMap mMap;
    private MarkerOptions place1, place2;
    private double rest_lat, rest_log;
    private double user_lat, user_log;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Bundle extra = getIntent().getBundleExtra(AddressActivity.EXTRA);
        if (extra != null) {
            rest_lat = extra.getDouble("REST_LAT");
            rest_log = extra.getDouble("REST_LNG");
            user_lat = extra.getDouble("USER_LAT");
            user_log = extra.getDouble("USER_LNG");

            place1 = new MarkerOptions().position(new LatLng(rest_lat, rest_log)).title("Restaurant");
            place2 = new MarkerOptions().position(new LatLng(user_lat, user_log)).title("Home");

            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.addMarker(place1);
        mMap.addMarker(place2);

        LatLng p1 = new LatLng(rest_lat, rest_log);
        LatLng p2 = new LatLng(user_lat, user_log);

        mMap.addPolyline(new PolylineOptions()
                .add(p1)
                .add(p2)
                .width(8f)
                .color(getResources().getColor(R.color.red)));

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(p1);
        builder.include(p2);
        LatLngBounds bounds = builder.build();

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, PADDING);
        mMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                CameraUpdate zout = CameraUpdateFactory.zoomBy(ZOOM);
                mMap.animateCamera(zout);
            }
            @Override
            public void onCancel() {

            }
        });
    }
}

