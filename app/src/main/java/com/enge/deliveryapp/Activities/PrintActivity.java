package com.enge.deliveryapp.Activities;

import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.print.PrintHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.enge.deliveryapp.Adapters.PrintItemListAdapter;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ActivityPrintBinding;
import com.enge.deliveryapp.model.OrderDetails;
import com.enge.deliveryapp.model.OrderItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class PrintActivity extends AppCompatActivity {

    public  static Bitmap bitmap;
    ActivityPrintBinding binding;
    PrintItemListAdapter adapter;
    ArrayList<OrderItem> orderItems;
    OrderDetails details = new OrderDetails();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_print);
        binding.setModel(new OrderDetails());

        Bundle extra = getIntent().getBundleExtra("extra");
        details = (OrderDetails) extra.getSerializable("details");
        orderItems = (ArrayList<OrderItem>) extra.getSerializable("orderItems");

        binding.setModel(details);

        setDate();
        initRecyclerView();
    }


    public void setDate() {

        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy  hh:mm");
        Calendar cal = Calendar.getInstance();
        String date = df.format(cal.getTime());
        ;
        binding.date.setText(date);
    }

    public void backToDetails(View view) {
        finish();
    }

    public void initRecyclerView() {

        binding.billRecycler.setLayoutManager(new LinearLayoutManager(this));

        adapter = new PrintItemListAdapter(this, orderItems);
        binding.billRecycler.setAdapter(adapter);

    }

    public void printBill(View view) {
        getBitmapFromView();
        doBillPrint();
    }

    private void getBitmapFromView() {
        bitmap = Bitmap.createBitmap(binding.ScrollLayout.getChildAt(0).getWidth(), binding.ScrollLayout.getChildAt(0).getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable bgDrawable = binding.ScrollLayout.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        binding.ScrollLayout.draw(canvas);

    }

    public void doBillPrint() {

        PrintHelper photoPrinter = new PrintHelper(this);
        photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);

        photoPrinter.printBitmap("bill.jpg", bitmap);
    }
}
