package com.enge.deliveryapp.Activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.enge.deliveryapp.Adapters.OrderItemsListAdapter;
import com.enge.deliveryapp.Enums.DeliveryStatusEnum;
import com.enge.deliveryapp.R;
import com.enge.deliveryapp.databinding.ActivityOrderItemBinding;
import com.enge.deliveryapp.model.Order;
import com.enge.deliveryapp.model.RestaurantData;
import com.enge.deliveryapp.model.UserData;
import com.enge.deliveryapp.viewModels.AddressViewModel;

import java.util.Observable;
import java.util.Observer;

public class OrderItemActivity extends AppCompatActivity implements Observer {

    // Create the observer which updates the UI.
    final android.arch.lifecycle.Observer<String> toastObserver = new android.arch.lifecycle.Observer<String>() {
        @Override
        public void onChanged(@Nullable final String msg) {

            Toast.makeText(OrderItemActivity.this, msg, Toast.LENGTH_LONG).show();
        }
    };

    final android.arch.lifecycle.Observer<Boolean> isTakeObserver = new android.arch.lifecycle.Observer<Boolean>() {
        @Override
        public void onChanged(@Nullable Boolean isLoading) {
            if (isLoading != null) {
                if (isLoading) {
                    binding.progressbar.setVisibility(View.VISIBLE);
                    binding.btnTakeOrder.setEnabled(false);

                } else {
                    binding.progressbar.setVisibility(View.GONE);
                    binding.btnTakeOrder.setEnabled(true);
                }

            }

        }
    };
    private ActivityOrderItemBinding binding;
    OrderItemsListAdapter orderItemsListAdapter;
    private RestaurantData restaurantData;
    private UserData userData;
    private int orderId;
    private String restaurantName;
    private String order_type;
    Order order;
    private AddressViewModel addressViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_item);
        binding.setModel(new RestaurantData());
        binding.setModel2(new UserData());

        addressViewModel = new AddressViewModel(OrderItemActivity.this);
        setUpObserver(addressViewModel);

        addressViewModel.getToastMessage().observe(this, toastObserver);
        addressViewModel.getIsTake().observe(this, isTakeObserver);


        Bundle extra = getIntent().getBundleExtra(AddressActivity.EXTRA);

        if (extra != null) {
            restaurantName = extra.getString("restaurantName");
            restaurantData = (RestaurantData) extra.getSerializable("restaurantData");
            userData = (UserData) extra.getSerializable("userData");
            order_type = extra.getString(AddressActivity.ORDER_TYPE);
            order = (Order) extra.getSerializable("ORDER");
            orderId = restaurantData.getOrderId();


            binding.txtRestaurantName.setText(restaurantName);
            binding.setModel(restaurantData);
            binding.setModel2(userData);
        }

        if(order_type.equals("Delivered") ||
                (order_type.equals("Late") && order.getDeliveryStatus() == DeliveryStatusEnum.DELIVRE.getCode())){
            binding.bottomLayout.setVisibility(View.INVISIBLE);
        }

        initRecyclerView();
    }

    /**
     * RecyclerView initialization && setup
     */
    public void initRecyclerView() {
        binding.orderItemList.setLayoutManager(new LinearLayoutManager(this));
        binding.orderItemList.setItemAnimator(new DefaultItemAnimator());
       // binding.orderItemList.addItemDecoration(new DividerItemDecoration(binding.orderItemList.getContext(), DividerItemDecoration.VERTICAL));
        binding.orderItemList.addItemDecoration(
                new DividerItemDecoration(this, new LinearLayoutManager(this).getOrientation()) {
                    @Override
                    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                        int position = parent.getChildAdapterPosition(view);
                        // hide the divider for the last child
                        if (position == parent.getAdapter().getItemCount() - 1) {
                            outRect.setEmpty();
                        } else {
                            super.getItemOffsets(outRect, view, parent, state);
                        }
                    }
                }
        );
        if( restaurantData != null){
            orderItemsListAdapter = new OrderItemsListAdapter(this, restaurantData.getMails(), order_type ,order.getDeliveryStatus());
            binding.orderItemList.setAdapter(orderItemsListAdapter);
        }

    }

    public void DeliverToCustomer(View view) {
        addressViewModel.takeOrder(orderId);
        Intent intent = new Intent(OrderItemActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }


    public void backToHome(View view) {
        Intent intent = new Intent(OrderItemActivity.this, HomeActivity.class) ;
        startActivity(intent);
        finish();
    }


    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof AddressViewModel) {
        }
    }

    private void setUpObserver(Observable observable) {
        observable.addObserver(this);
    }
}

